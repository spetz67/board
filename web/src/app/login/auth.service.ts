import {Injectable} from '@angular/core';
import {GoogleLoginProvider, SocialAuthService} from "angularx-social-login";
import {Router} from "@angular/router";
import {LoginService} from "@board-api/services/login.service";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    //todo refresh token before or at expiration

    accessToken: string;
    accessTokenExpirationDate: Date;

    constructor(private router: Router,
                private loginService: LoginService,
                private socialAuthService: SocialAuthService) {
    }

    loginWithGoogle(): void {
        this.socialAuthService
                .signIn(GoogleLoginProvider.PROVIDER_ID)
                .then(user => {
                    this.loginService
                            .loginGoogle({body: user.idToken})
                            .subscribe(accessRefreshToken => {
                                this.accessTokenExpirationDate = new Date(Date.now()+accessRefreshToken.expires_in*1000)
                                this.accessToken = accessRefreshToken.access_token

                                if (accessRefreshToken.roles?.includes('ROLE_ADMIN'))
                                    this.router.navigate(['user-overview']);
                                else
                                    this.router.navigate(['user-home', accessRefreshToken.username]);
                            })
                })
    }

    logout() {
        this.socialAuthService
                .signOut(true)
                .then(() => {
                    this.accessToken = null
                    this.accessTokenExpirationDate = null
                    return this.router.navigate(['']);
                })
    }

    isAccessTokenExpired() {
        const expirationDate = this.accessTokenExpirationDate;
        return expirationDate
                ? Date.now() > expirationDate.getTime()
                : false
    }
}
