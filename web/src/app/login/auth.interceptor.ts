import {Injectable} from '@angular/core';
import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpStatusCode
} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {AuthService} from "./auth.service";
import {catchError} from "rxjs/operators";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService) {
    }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

        const accessToken = this.authService.accessToken;
        if (accessToken) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${accessToken}`
                }
            });
        }

        return next.handle(request).pipe(
                catchError((error: HttpErrorResponse) => {
                    if (error.status==HttpStatusCode.Unauthorized && this.authService.isAccessTokenExpired()) {
                        return throwError(new Error("Session expired, please logout and login again."))
                    }
                    return throwError(error)
                })
        )
    }
}
