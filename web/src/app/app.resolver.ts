import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";
import {FolloweeOut, FollowerOut, Note, OtherUserOut, UserOut} from "@board-api/models";
import {FollowingService, NotesService, UsersService} from "@board-api/services";

function getUserId(route: ActivatedRouteSnapshot) {
    return route.paramMap.get('id') || route.parent.paramMap.get('id');
}

@Injectable({providedIn: 'root'})
export class UserIdResolver implements Resolve<string> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string {
        return getUserId(route)
    }
}

@Injectable({providedIn: 'root'})
export class UserOthersResolver implements Resolve<OtherUserOut[]> {
    constructor(private usersService: UsersService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OtherUserOut[]> {
        return this.usersService
                .getOthers({userId: getUserId(route)});
    }
}

@Injectable({providedIn: 'root'})
export class UserDetailsResolver implements Resolve<UserOut> {
    constructor(private usersService: UsersService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserOut> {
        return this.usersService
                .getUserById({id: getUserId(route)});
    }
}

@Injectable({providedIn: 'root'})
export class UserFolloweesResolver implements Resolve<FolloweeOut[]> {
    constructor(private followingService: FollowingService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FolloweeOut[]> {
        return this.followingService
                .getFollowees({userId: getUserId(route)})
                .pipe(map(page => page.content));
    }
}

@Injectable({providedIn: 'root'})
export class UserFollowersResolver implements Resolve<FollowerOut[]> {
    constructor(private followingService: FollowingService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FollowerOut[]> {
        return this.followingService
                .getFollowers({userId: getUserId(route)})
                .pipe(map(page => page.content));
    }
}

@Injectable({providedIn: 'root'})
export class UserNotesResolver implements Resolve<Note[]> {
    constructor(private notesService: NotesService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Note[]> {
        return this.notesService
                .getNotes({userId: getUserId(route)})
                .pipe(map(page => page.content));
    }
}