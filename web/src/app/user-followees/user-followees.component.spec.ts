import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFolloweesComponent } from './user-followees.component';

describe('UserFolloweesComponent', () => {
  let component: UserFolloweesComponent;
  let fixture: ComponentFixture<UserFolloweesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserFolloweesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFolloweesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
