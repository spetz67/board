import {Component} from '@angular/core';
import {FollowerOut} from "@board-api/models";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-user-followers',
  templateUrl: './user-followers.component.html',
  styleUrls: ['./user-followers.component.css']
})
export class UserFollowersComponent {

  followers: FollowerOut[] = [];

  constructor(private route: ActivatedRoute) {

    this.followers = this.route.snapshot.data.followers;
  }
}
