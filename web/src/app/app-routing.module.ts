import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {UserDetailsComponent} from "./user-details/user-details.component";
import {UserHomeComponent} from "./user-home/user-home.component";
import {UserNotesComponent} from "./user-notes/user-notes.component";
import {UserFolloweesComponent} from "./user-followees/user-followees.component";
import {UserFollowersComponent} from "./user-followers/user-followers.component";
import {UserOthersComponent} from "./user-others/user-others.component";
import {
    UserDetailsResolver,
    UserFolloweesResolver,
    UserFollowersResolver,
    UserIdResolver,
    UserNotesResolver,
    UserOthersResolver
} from "./app.resolver";
import {LoginComponent} from "./login/login.component";
import {AuthGuard} from "./login/auth.guard";

const userHomeRoutes: Routes = [
    {path: 'notes', component: UserNotesComponent,
        resolve: {userId: UserIdResolver, notes: UserNotesResolver}},
    {path: 'followees', component: UserFolloweesComponent,
        resolve: {userId: UserIdResolver, followees: UserFolloweesResolver}},
    {path: 'followers', component: UserFollowersComponent,
        resolve: {userId: UserIdResolver, followers: UserFollowersResolver}},
    {path: 'others', component: UserOthersComponent,
        resolve: {userId: UserIdResolver, otherUsers: UserOthersResolver}},
];

const routes: Routes = [
    {path: '', redirectTo: '/login', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'user-overview', component: UsersComponent, canActivate: [AuthGuard]},
    {path: 'add-user', component: UserDetailsComponent, canActivate: [AuthGuard]},
    {path: 'edit-user/:id', component: UserDetailsComponent, canActivate: [AuthGuard],
        resolve: {userId: UserIdResolver, user: UserDetailsResolver}},
    {path: 'user-home/:id', component: UserHomeComponent, canActivate: [AuthGuard], children: userHomeRoutes},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

