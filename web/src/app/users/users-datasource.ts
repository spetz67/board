import {DataSource} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Observable, ReplaySubject} from 'rxjs';
import {UsersService} from "@board-api/services";
import {EmailContact, PhoneContact} from "@board-api/models";

export type UsersItem = {
    id?: string;
    name: string;
    totalWordCount?: number;
    followerCount?: number;
    phoneContacts?: Array<PhoneContact>;
    emailContacts?: Array<EmailContact>;
}

/**
 * Data source for the Users view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class UsersDataSource extends DataSource<UsersItem> {

    private observable: ReplaySubject<UsersItem[]> = new ReplaySubject<UsersItem[]>();

    constructor(private usersService: UsersService,
                private sort: MatSort,
                private paginator: MatPaginator) {
        super();

        paginator.page.subscribe(() => this.reload())
        sort.sortChange.subscribe(() => this.reload())
    }

    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect(): Observable<UsersItem[]> {
        this.reload()
        return this.observable
    }

    /**
     *  Called when the table is being destroyed. Use this function, to clean up
     * any open connections or free any held resources that were set up during connect.
     */
    disconnect() {
    }

    reload() {
        const params = {
            page: this.paginator.pageIndex,
            size: this.paginator.pageSize,
            sort: UsersDataSource.getSortQueryParameter(this.sort)
        };

        this.usersService
                .getUsers(params)
                .subscribe(users => {
                    this.observable.next(users.content);
                    this.paginator.length = users.totalElements
                });
    }

    private static getSortQueryParameter(sort: MatSort): string[] | null {

        if (sort.direction == null || sort.direction == '')
            return null;

        const columnToProperty = {
            'name': 'name',
            'totalWordCount': 'totalWordCount'
        }

        let sortParameter = columnToProperty[sort.active];
        if (!sortParameter)
            throw new Error("Unsupported sort column: " + sort.active)

        sortParameter = `${sortParameter}:${sort.direction}`

        return [ sortParameter ]
    }
}
