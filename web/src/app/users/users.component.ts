import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTable} from '@angular/material/table';
import {UsersDataSource, UsersItem} from './users-datasource';
import {UsersService} from "@board-api/services";
import {Router} from "@angular/router";
import {AuthService} from "../login/auth.service";

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})
export class UsersComponent implements AfterViewInit {
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatTable) table: MatTable<UsersItem>;
    dataSource: UsersDataSource;
    displayedColumns = ['data', 'action'];

    constructor(private usersService: UsersService,
                private router: Router,
                private authService: AuthService) {
    }

    ngAfterViewInit() {
        this.dataSource = new UsersDataSource(this.usersService, this.sort, this.paginator);
        this.table.dataSource = this.dataSource;
    }

    deleteUser(user: UsersItem) {
        this.usersService
                .deleteUser({id: user.id})
                .subscribe(() => this.dataSource.reload())
    }

    logout() {
        this.authService.logout()
    }
}
