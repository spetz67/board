import {Component} from '@angular/core';
import {FollowingService} from "@board-api/services";
import {OtherUserOut} from "@board-api/models";
import {ActivatedRoute} from "@angular/router";
import {MatCheckboxChange} from "@angular/material/checkbox";

@Component({
    selector: 'app-user-others',
    templateUrl: './user-others.component.html',
    styleUrls: ['./user-others.component.css']
})
export class UserOthersComponent {

    otherUsers: OtherUserOut[] = [];

    private userId: string;

    constructor(private followingService: FollowingService,
                private route: ActivatedRoute) {

        this.userId = this.route.snapshot.data.userId
        this.otherUsers = this.route.snapshot.data.otherUsers;
    }

    followingChanged(otherUser: OtherUserOut, index: number, event: MatCheckboxChange) {
        const observable = event.checked
                ? this.followingService.follow({userId: this.userId, followeeUserId: otherUser.id})
                : this.followingService.unfollow({userId: this.userId, followeeUserId: otherUser.id})
        observable.subscribe(() => this.otherUsers[index].canFollow = !event.checked)
    }
}
