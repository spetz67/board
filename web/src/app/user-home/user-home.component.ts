import {Component, OnInit} from '@angular/core';
import {UserOut} from "@board-api/models";
import {UsersService} from "@board-api/services";
import {Location} from "@angular/common";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../login/auth.service";

@Component({
    selector: 'app-user-home',
    templateUrl: './user-home.component.html',
    styleUrls: ['./user-home.component.css']
})
export class UserHomeComponent implements OnInit {

    userId: string
    userName: string

    constructor(private usersService: UsersService,
                private location: Location,
                private router: Router,
                private route: ActivatedRoute,
                private authService: AuthService) {
        this.userId = this.route.snapshot.paramMap.get('id');
    }

    ngOnInit() {
        this.usersService
                .getUserById({id: this.userId})
                .subscribe(userOut => this.userName = userOut.name)

        // navigate to notes when not navigated to a child -> todo: find better solution
        if (this.location.isCurrentPathEqualTo(this.location.prepareExternalUrl(this.route.snapshot.url.join('/'))))
            this.router.navigate(['notes'], { replaceUrl: true, relativeTo: this.route })
    }

    goBack() {
        this.location.back()
    }

    isActive(path: string) {
        return this.location.path().endsWith('/'+path)
    }

    logout() {
        this.authService.logout()
    }
}
