import {Component} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Note} from "@board-api/models";
import {NotesService} from "@board-api/services";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-user-notes',
    templateUrl: './user-notes.component.html',
    styleUrls: ['./user-notes.component.css']
})
export class UserNotesComponent {

    newNoteTextArea: FormControl = new FormControl()
    notes: Note[] = [];

    private userId: string;

    constructor(private notesService: NotesService,
                private route: ActivatedRoute) {

        this.userId = this.route.snapshot.data.userId
        this.notes = this.route.snapshot.data.notes;
    }

    postNote() {
        this.notesService
                .postNote({userId: this.userId, body: {text: this.newNoteTextArea.value}})
                .subscribe(value => {
                    this.newNoteTextArea.setValue('');
                    this.loadNotes()
                })
    }

    deleteNote(number: number) {
        this.notesService
                .deleteNoteByNumber({userId: this.userId, number: number})
                .subscribe(() => this.loadNotes())
    }

    private loadNotes() {
        this.notesService
                .getNotes({userId: this.userId})
                .subscribe(notes => this.notes = notes.content)
    }
}
