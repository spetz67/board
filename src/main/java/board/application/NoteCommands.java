package board.application;

import board.application.note.NoteIn;
import board.application.note.NotesExceededException;
import board.application.user.UserNotFoundException;
import board.domain.note.Note;
import board.domain.note.NoteConfiguration;
import board.domain.note.NoteFactory;
import board.domain.note.NoteRepository;
import io.micronaut.retry.annotation.Retryable;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class NoteCommands
{
    private final NoteRepository repository;
    private final NoteFactory factory;
    private final NoteConfiguration configuration;

    @Retryable(includes = RevisionMismatchException.class)
    public Note postNote(UUID userId, NoteIn noteIn) throws UserNotFoundException
    {
        int noteCount = repository.getNoteCountByUserId(userId);
        if (configuration.exceedsMaxNotes(noteCount+1))
            throw new NotesExceededException(configuration.getMaxNotesPerUser());

        Note note = factory.createNote(userId, noteIn.text());
        repository.add(note);
        return note;
    }

    @Retryable(includes = RevisionMismatchException.class)
    public void deleteNoteByUserAndNumber(UUID userId, int noteNumber)
    {
        repository.delete(userId, noteNumber);
    }
}