package board.application.note;

import board.domain.common.TemplatedException;

import java.util.Map;

public class NoteNotFoundException extends TemplatedException
{
    public NoteNotFoundException(int number)
    {
        super("error.user.noteNotFoundByNumber", Map.of("number", number));
    }
}
