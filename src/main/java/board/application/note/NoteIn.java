package board.application.note;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;

@Introspected
public record NoteIn(@NotBlank String text)
{
}
