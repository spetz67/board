package board.application;

import java.util.List;

public record Pageable(int size, int number, List<Order> orderBy)
{
    public record Order(String property, boolean ascending) { }

    public static Pageable unpaged()
    {
        return new Pageable(Integer.MAX_VALUE, 0, List.of());
    }

    public boolean sorted()
    {
        return orderBy!=null && !orderBy.isEmpty();
    }

    public int offset()
    {
        return number*size;
    }

    public Pageable order(String property)
    {
        return new Pageable(size, number, List.of(new Order(property, true)));
    }
}
