package board.application;

import board.application.note.NoteNotFoundException;
import board.domain.note.Note;

import java.util.UUID;

public interface NoteQueryRepository
{
    Page<Note> queryNotesByUserId(UUID userId, Integer fromNumber, Pageable pageable);

    Note queryNoteByUserIdAndNumber(UUID userId, int number) throws NoteNotFoundException;
}
