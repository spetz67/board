package board.application.user;

import board.domain.user.EmailContact;
import board.domain.user.PhoneContact;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRawValue;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Introspected
public record UserOut(
        @NotNull
        UUID id,

        @JsonIgnore
        Instant updatedInstant,

        int totalWordCount,

        int followerCount,

        @Schema(description = "The user name")
        @NotBlank
        String name,

        @JsonRawValue
        @JsonInclude(JsonInclude.Include.NON_NULL)
        @Schema(implementation = PhoneContactList.class)
        @ArraySchema(schema = @Schema(implementation = PhoneContact.class))
        String phoneContacts,

        @JsonRawValue
        @JsonInclude(JsonInclude.Include.NON_NULL)
        @Schema(implementation = EmailContactList.class)
        @ArraySchema(schema = @Schema(implementation = EmailContact.class))
        String emailContacts
)
{
    // workaround interfaces to create correct openapi file
    private interface PhoneContactList extends List<PhoneContact> { }
    private interface EmailContactList extends List<EmailContact> { }
}

