package board.application.user;

import board.domain.user.EmailContact;
import board.domain.user.PhoneContact;
import board.domain.user.User;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Introspected
public record UserIn(@Schema(description = "The user name") @NotBlank String name,
                     List<PhoneContact> phoneContacts,
                     List<EmailContact> emailContacts)
{
    public void copyTo(User user)
    {
        user.setName(name);

        user.setPhoneContacts(phoneContacts!=null
                ? phoneContacts
                : List.of());

        user.setEmailContacts(emailContacts!=null
                ? emailContacts
                : List.of());
    }
}
