package board.application.user;

import board.domain.common.TemplatedException;

import java.util.Map;
import java.util.UUID;

public class UserNotFoundException extends TemplatedException
{
    public UserNotFoundException(UUID userId)
    {
        super("error.user.userNotFoundById", Map.of("userId", userId));
    }
}
