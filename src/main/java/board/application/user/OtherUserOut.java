package board.application.user;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Introspected
public record OtherUserOut(@NotNull UUID id,
                           @NotEmpty String name,
                           boolean canFollow)
{
}
