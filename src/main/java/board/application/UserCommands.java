package board.application;

import board.application.user.UserIn;
import board.application.user.UserNotFoundException;
import board.application.user.UserOut;
import board.domain.user.User;
import board.domain.user.UserFactory;
import board.domain.user.UserRepository;
import io.micronaut.retry.annotation.Retryable;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class UserCommands
{
    private final UserRepository repository;
    private final UserFactory factory;
    private final UserQueryRepository queryRepository;

    public UserOut registerUser(UserIn userIn)
    {
        User user = factory.createUser(userIn.name());
        userIn.copyTo(user);
        user = repository.add(user);
        return queryRepository.queryUserById(user.getId());
    }

    @Retryable(includes = RevisionMismatchException.class)
    public void updateUser(UUID userId, UserIn userIn) throws UserNotFoundException
    {
        User user = repository.getById(userId);
        userIn.copyTo(user);
        repository.update(user);
    }

    public void deleteUser(UUID userId)
    {
        repository.delete(userId);
    }
}