package board.application;

import board.application.follow.FolloweeOut;
import board.application.follow.FollowerOut;

import java.util.UUID;

public interface FollowQueryRepository
{
    Page<FollowerOut> queryFollowersByUserId(UUID userId, Pageable pageable);

    FollowerOut queryFollowerByUserAndFollowerUserId(UUID userId, UUID followerUserId);

    Page<FolloweeOut> queryFolloweesByUserId(UUID userId, Pageable pageable);

    FolloweeOut queryFolloweeByUserAndFolloweeUserId(UUID userId, UUID followeeUserId);
}
