package board.application;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public record Page<T>(List<T> content, Pageable pageable, long totalSize)
{
    public int size()
    {
        return pageable.size();
    }

    public int totalPages()
    {
        int size = size();
        return (int) Math.ceil((double)totalSize()/(double) size);
    }

    public int pageNumber()
    {
        return pageable.number();
    }

    public <T2> Page<T2> map(Function<T, T2> function)
    {
        List<T2> content = content().stream()
                .map(function)
                .collect(Collectors.toList());
        return new Page<>(content, pageable(), totalSize());
    }
}
