package board.application.follow;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Introspected
public record FollowerOut(@NotNull UUID userId,
                          @NotNull Instant createdInstant,
                          @NotEmpty String userName)
{
}
