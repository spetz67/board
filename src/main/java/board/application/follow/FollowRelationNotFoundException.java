package board.application.follow;

import board.domain.common.TemplatedException;

import java.util.Map;
import java.util.UUID;

public class FollowRelationNotFoundException extends TemplatedException
{
    public FollowRelationNotFoundException(UUID follower, UUID followee)
    {
        super("error.follow.followRelationNotFound", Map.of("follower", follower, "followee", followee));
    }
}
