package board.application;

import board.application.user.OtherUserOut;
import board.application.user.UserNotFoundException;
import board.application.user.UserOut;

import java.util.UUID;
import java.util.stream.Stream;

public interface UserQueryRepository
{
    Page<UserOut> queryUsers(Pageable pageable);

    UserOut queryUserById(UUID id) throws UserNotFoundException;

    Stream<OtherUserOut> queryOtherUsersByUserId(UUID userId, Pageable pageable);
}
