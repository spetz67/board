package board.application;

import board.domain.follow.FollowRelation;
import board.domain.follow.FollowRelationFactory;
import board.domain.follow.FollowRelationRepository;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class FollowCommands
{
    private final FollowRelationRepository repository;
    private final FollowRelationFactory factory;

    public void follow(UUID userId, UUID followeeUserId)
    {
        FollowRelation followRelation = factory.createFollowRelation(userId, followeeUserId);
        repository.add(followRelation);
    }

    public void unfollow(UUID userId, UUID followeeUserId)
    {
        repository.delete(userId, followeeUserId);
    }

    public void resetFolloweeFirstUnreadNoteNumber(UUID userId, UUID followeeUserId)
    {
        FollowRelation followRelation = repository.get(userId, followeeUserId);
        followRelation.resetFolloweeFirstUnreadNoteNumber();
        repository.update(followRelation);
    }
}