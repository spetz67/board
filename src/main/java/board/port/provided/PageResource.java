package board.port.provided;

import board.application.Page;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

public record PageResource<T>(@JsonIgnore Page<T> page)
{
    @Schema(description = "The page size, maximum number of elements per page")
    public int getSize()
    {
        return page.size();
    }

    @Schema(description = "The total number of result elements")
    public long getTotalElements()
    {
        return page.totalSize();
    }

    @Schema(description = "The total number of pages")
    public int getTotalPages()
    {
        return page.totalPages();
    }

    @Schema(description = "The number of this page, starting at 0")
    public int getNumber()
    {
        return page.pageNumber();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public List<T> getContent()
    {
        return page.content();
    }
}
