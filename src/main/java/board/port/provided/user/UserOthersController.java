package board.port.provided.user;

import board.application.UserQueryRepository;
import board.application.user.OtherUserOut;
import board.port.provided.ApiCommonResponses;
import board.port.provided.PageableParameter;
import board.port.provided.TransactionalStreamWriter;
import io.micronaut.core.io.Writable;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.RequestBean;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Controller("/api/users/{userId}/others")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@ExecuteOn(TaskExecutors.IO)
@ApiCommonResponses
@Tag(name = "Users")
public class UserOthersController
{
    private final TransactionalStreamWriter streamWriter;
    private final UserQueryRepository queries;

    @Get
    @ApiResponse(responseCode = "200", description = "Success",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = OtherUserOut.class))))
    public Writable getOthers(@NotNull UUID userId, @RequestBean PageableParameter pageableParameter)
    {
        return streamWriter.getWritable(queries.queryOtherUsersByUserId(userId, pageableParameter.toPageable()));
    }
}