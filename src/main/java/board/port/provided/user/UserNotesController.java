package board.port.provided.user;

import board.application.NoteCommands;
import board.application.NoteQueryRepository;
import board.application.Page;
import board.application.note.NoteIn;
import board.application.note.NoteNotFoundException;
import board.domain.note.Note;
import board.port.provided.ApiCommonResponses;
import board.port.provided.PageResource;
import board.port.provided.PageableParameter;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.server.util.HttpHostResolver;
import io.micronaut.http.uri.UriBuilder;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.UUID;

@Controller("/api/users/{userId}/notes")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@Slf4j
@ExecuteOn(TaskExecutors.IO)
@ApiCommonResponses
@Tag(name = "Notes")
public class UserNotesController
{
    private final HttpHostResolver hostResolver;
    private final NoteQueryRepository queries;
    private final NoteCommands commands;

    @Get
    public PageResource<Note> getNotes(@NotNull UUID userId, @QueryValue @Nullable Integer fromNumber,
            @RequestBean PageableParameter pageableParameter)
    {
        Page<Note> page = queries.queryNotesByUserId(userId, fromNumber, pageableParameter.toPageable());
        return new PageResource<>(page);
    }

    @Get("/{number}")
    public Note getNoteByNumber(@NotNull UUID userId, int number)
    {
        return queries.queryNoteByUserIdAndNumber(userId, number);
    }

    @Delete("/{number}")
    @Status(HttpStatus.NO_CONTENT)
    public void deleteNoteByNumber(@NotNull UUID userId, int number)
    {
        try
        {
            commands.deleteNoteByUserAndNumber(userId, number);
        }
        catch (NoteNotFoundException e)
        {
            // REST DELETE must not fail when resource is not there, so just log this
            log.warn("Note {} not found with user {}", number, userId);
        }
    }

    @Post
    @ApiResponse(responseCode = "201", description = "Resource created",
            headers = @Header(name = "location", description = "The URI of the created resource"))
    public HttpResponse<Void> postNote(@NotNull UUID userId, @Body @Valid NoteIn noteIn, HttpRequest<?> request)
    {
        Note note = commands.postNote(userId, noteIn);
        URI location = UriBuilder.of(hostResolver.resolve(request))
                .path(request.getPath())
                .path(String.valueOf(note.number()))
                .build();
        return HttpResponse.created(location);
    }
}