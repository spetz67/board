package board.port.provided.user;

import board.application.UserCommands;
import board.application.UserQueryRepository;
import board.application.user.UserIn;
import board.application.user.UserOut;
import board.port.provided.ApiCommonResponses;
import board.port.provided.PageResource;
import board.port.provided.PageableParameter;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.server.util.HttpHostResolver;
import io.micronaut.http.uri.UriBuilder;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.UUID;

@Controller("/api/users")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@ExecuteOn(TaskExecutors.IO)
@ApiCommonResponses
@Tag(name = "Users")
public class UsersController
{
    private final HttpHostResolver hostResolver;
    private final UserQueryRepository queries;
    private final UserCommands commands;

    /**
     * Get a page of users.
     * @return page of user resources
     */
    @Get
    public PageResource<UserOut> getUsers(@RequestBean PageableParameter pageableParameter)
    {
        return new PageResource<>(queries.queryUsers(pageableParameter.toPageable()));
    }

    /**
     * Get a user by id.
     * @return the user resource
     */
    @Get(uri = "/{id}")
    public HttpResponse<UserOut> getUserById(@NotNull UUID id)
    {
        UserOut user = queries.queryUserById(id);
        return HttpResponse
                .ok(user)
                .headers(headers -> headers.lastModified(user.updatedInstant().toEpochMilli()));
    }

    /**
     * Add a new user.
     * @param userIn the user attributes
     */
    @Post
    @ApiResponse(responseCode = "201", description = "Resource created",
            headers = @Header(name = "location", description = "The URI of the created resource"))
    public HttpResponse<Void> createUser(@Body @Valid UserIn userIn, HttpRequest<?> request)
    {
        UserOut user = commands.registerUser(userIn);
        URI location = UriBuilder.of(hostResolver.resolve(request))
                .path(request.getPath())
                .path(String.valueOf(user.id()))
                .build();
        return HttpResponse.created(location);
    }

    /**
     * Update an existing user.
     * @param id the user id
     * @param userIn the user attributes
     */
    @Put(uri = "/{id}")
    @Status(HttpStatus.NO_CONTENT)
    public void updateUser(@NotNull UUID id, @Body @Valid UserIn userIn)
    {
        commands.updateUser(id, userIn);
    }

    /**
     * Delete a user.
     * @param id the user id
     */
    @Delete(uri = "/{id}")
    @Status(HttpStatus.NO_CONTENT)
    public void deleteUser(@NotNull UUID id)
    {
        commands.deleteUser(id);
    }
}