package board.port.provided;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.core.io.Writable;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.Writer;
import java.util.stream.Stream;

@Singleton
@RequiredArgsConstructor
public class TransactionalStreamWriter
{
    private final ObjectMapper objectMapper;

    public Writable getWritable(Stream<?> stream)
    {
        return writer -> writeStream(writer, stream);
    }

    @Transactional
    void writeStream(Writer writer, Stream<?> value) throws IOException
    {
        try (JsonGenerator generator = objectMapper.createGenerator(writer);
             Stream<?> closableValue = value)
        {
            generator.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
            objectMapper.writeValue(generator, closableValue);
        }
    }
}