package board.port.provided;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@ApiResponse(responseCode = "4XX", description = "Bad request",
        content = @Content(schema = @Schema(type = "object")))  //todo provide openapi friendly error schema
@ApiResponse(responseCode = "5XX", description = "Server error",
        content = @Content(schema = @Schema(type = "object")))
@ApiResponse(responseCode = "400", description = "Validation error",
        content = @Content(schema = @Schema(type = "object")))
@ApiResponse(description = "")  // needed to generate required description entries
@SecurityRequirement(name = "bearerAuth")
public @interface ApiCommonResponses
{
}
