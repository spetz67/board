package board.port.provided.auth;

import board.domain.common.TemplatedException;

import java.util.Map;

public class CredentialsNotFoundException extends TemplatedException
{
    public CredentialsNotFoundException(String emailAddress)
    {
        super("error.auth.credentialsNotFound", Map.of("emailAddress", emailAddress));
    }
}
