package board.port.provided.auth;

import board.application.AuthQueryRepository;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.handlers.LoginHandler;
import io.micronaut.security.token.jwt.render.BearerAccessRefreshToken;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.UUID;

@Controller("/login-google")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@ExecuteOn(TaskExecutors.IO)
@Slf4j
@ApiResponse(responseCode = "4XX", description = "Bad request",
        content = @Content(schema = @Schema(type = "object")))  //todo provide openapi friendly error schema
@ApiResponse(responseCode = "5XX", description = "Server error",
        content = @Content(schema = @Schema(type = "object")))
@Tag(name = "Login")
class LoginGoogleController
{
    private final LoginHandler loginHandler;
    private final GoogleLoginVerifier googleLoginVerifyer;
    private final AuthQueryRepository authQueryRepository;

    @SuppressWarnings("unchecked")
    @Post
    @Consumes(MediaType.TEXT_PLAIN)
    HttpResponse<BearerAccessRefreshToken> loginGoogle(@Body String idToken, HttpRequest<?> request) throws GeneralSecurityException
    {
        try
        {
            Authentication authentication = getAuthentication(idToken);
            HttpResponse<?> response = loginHandler.loginSuccess(authentication, request);
            return (HttpResponse<BearerAccessRefreshToken>) response;
        }
        catch (IOException|IllegalArgumentException e)
        {
            log.error("Verifying token failed.", e);
            throw new GeneralSecurityException("Verifying token failed");
        }
    }

    private Authentication getAuthentication(String idToken) throws GeneralSecurityException, IOException
    {
        String emailAddress = googleLoginVerifyer.getEmailAddress(idToken);

        if (authQueryRepository.isAdmin(emailAddress))
            return Authentication.build(emailAddress, List.of("ROLE_ADMIN", "ROLE_USER"));

        String userId = authQueryRepository
                .findUserIdByEmail(emailAddress)
                .map(UUID::toString)
                .orElseThrow(() -> new CredentialsNotFoundException(emailAddress));
        return Authentication.build(userId, List.of("ROLE_USER"));
    }
}