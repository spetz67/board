package board.port.provided.auth;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.util.Utils;
import lombok.extern.slf4j.Slf4j;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Singleton;
import javax.security.auth.login.FailedLoginException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

@Singleton
@Slf4j
class GoogleLoginVerifier
{
    private GoogleIdTokenVerifier verifier;

    @PostConstruct
    void init()
    {
        verifier = new GoogleIdTokenVerifier.Builder(Utils.getDefaultTransport(), Utils.getDefaultJsonFactory())
                // Specify the CLIENT_ID of the app that accesses the backend:
                .setAudience(List.of("506924129809-2hnf3rn0l6hbsnnumqp4v1btunmglher.apps.googleusercontent.com"))
                .build();
    }

    String getEmailAddress(String idTokenString) throws GeneralSecurityException, IOException
    {
        GoogleIdToken idToken = verifier.verify(idTokenString);
        if (idToken==null)
        {
            log.error("Verifying token failed.");
            throw new FailedLoginException("Invalid token");
        }

        GoogleIdToken.Payload payload = idToken.getPayload();
        return payload.getEmail();
    }
}
