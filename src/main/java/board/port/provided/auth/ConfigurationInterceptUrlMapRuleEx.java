package board.port.provided.auth;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.http.HttpMethod;
import io.micronaut.security.config.InterceptUrlMapPattern;
import io.micronaut.security.config.SecurityConfiguration;
import io.micronaut.security.rules.ConfigurationInterceptUrlMapRule;
import io.micronaut.security.token.RolesFinder;
import io.micronaut.security.utils.SecurityService;

import jakarta.inject.Singleton;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
@Replaces(ConfigurationInterceptUrlMapRule.class)
class ConfigurationInterceptUrlMapRuleEx extends ConfigurationInterceptUrlMapRule
{
    private static final String PRINCIPAL_TOKEN = "@PRINCIPAL@";

    private SecurityService securityService;

    public ConfigurationInterceptUrlMapRuleEx(RolesFinder rolesFinder, SecurityConfiguration securityConfiguration,
            SecurityService securityService)
    {
        super(rolesFinder, securityConfiguration);

        this.securityService = securityService;
    }

    @Override
    protected List<InterceptUrlMapPattern> getPatternList()
    {
        List<InterceptUrlMapPattern> patternList = super.getPatternList();
        Optional<String> username = securityService.username();

        if (username.isEmpty())
            return patternList;

        return patternList
                .stream()
                .map(interceptUrlMapPattern -> resolvePrincipal(interceptUrlMapPattern, username.get()))
                .collect(Collectors.toList());
    }

    private InterceptUrlMapPattern resolvePrincipal(InterceptUrlMapPattern interceptUrlMapPattern, String userId)
    {
        String pattern = interceptUrlMapPattern.getPattern();

        if (pattern.contains(PRINCIPAL_TOKEN))
        {
            String patternWithPrincipalResolved = pattern.replace(PRINCIPAL_TOKEN, userId);
            List<String> access = interceptUrlMapPattern.getAccess();
            HttpMethod httpMethod = interceptUrlMapPattern.getHttpMethod().orElse(null);
            return new InterceptUrlMapPattern(patternWithPrincipalResolved, access, httpMethod);
        }

        return interceptUrlMapPattern;
    }
}
