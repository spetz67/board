package board.port.provided;

import board.application.Pageable;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.http.annotation.QueryValue;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.Value;

import java.util.List;
import java.util.stream.Collectors;

@Value
@Introspected
public class PageableParameter
{
    @QueryValue(defaultValue = "0")
    @Parameter(required = false, description = "Requested page number, starting with 0")    // "required" needed for openapi
    private int page;

    @QueryValue(defaultValue = "100")
    @Parameter(required = false, description = "Requested page size")    // "required" needed for openapi
    private int size;

    @Nullable
    @Parameter(description = "Sort property names, optionally followed by :asc or :desc")
    private List<String> sort;

    public Pageable toPageable()
    {
        List<Pageable.Order> orderBy = sort!=null
                ? sort.stream().map(this::sortToOrder).collect(Collectors.toList())
                : List.of();
        return new Pageable(size, page, orderBy);
    }

    private Pageable.Order sortToOrder(String sort)
    {
        int separatorIndex = sort.indexOf(':');
        if (separatorIndex<0)
            return new Pageable.Order(sort, true);
        String direction = sort.substring(separatorIndex+1);
        if (direction.equalsIgnoreCase("asc"))
            return new Pageable.Order(sort.substring(0, separatorIndex), true);
        if (direction.equalsIgnoreCase("desc"))
            return new Pageable.Order(sort.substring(0, separatorIndex), false);
        throw new IllegalArgumentException("Invalid sort parameter");
    }
}
