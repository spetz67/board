package board.port.provided.follow;

import board.application.FollowCommands;
import board.application.FollowQueryRepository;
import board.application.follow.FolloweeOut;
import board.port.provided.ApiCommonResponses;
import board.port.provided.PageResource;
import board.port.provided.PageableParameter;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.server.util.HttpHostResolver;
import io.micronaut.http.uri.UriBuilder;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.UUID;

@Controller("/api/users/{userId}/followees")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@ExecuteOn(TaskExecutors.IO)
@ApiCommonResponses
@Tag(name = "Following")
public class UserFolloweesController
{
    private final HttpHostResolver hostResolver;
    private final FollowQueryRepository queries;
    private final FollowCommands commands;

    @Get
    public PageResource<FolloweeOut> getFollowees(@NotNull UUID userId,
            @RequestBean PageableParameter pageableParameter)
    {
        var page = queries.queryFolloweesByUserId(userId, pageableParameter.toPageable());
        return new PageResource<>(page);
    }

    @Get("/{followeeUserId}")
    public FolloweeOut getFolloweeByUserId(@NotNull UUID userId, @NotNull UUID followeeUserId)
    {
        return queries.queryFolloweeByUserAndFolloweeUserId(userId, followeeUserId);
    }

    @Post("/{followeeUserId}")
    @ApiResponse(responseCode = "201", description = "Resource created",
            headers = @Header(name = "location", description = "The URI of the created resource"))
    public HttpResponse<Void> follow(@NotNull UUID userId, @NotNull UUID followeeUserId, HttpRequest<?> request)
    {
        commands.follow(userId, followeeUserId);
        URI location = UriBuilder.of(hostResolver.resolve(request))
                .path(request.getPath())
                .build();
        return HttpResponse.created(location);
    }

    @Patch("/{followeeUserId}")
    @Status(HttpStatus.NO_CONTENT)
    public void resetNewNoteAvailable(@NotNull UUID userId, @NotNull UUID followeeUserId,
            @Body ResetNewNoteAvailablePatchBody body)
    {
        if (Boolean.FALSE == body.getNewNoteAvailable())
            commands.resetFolloweeFirstUnreadNoteNumber(userId, followeeUserId);
    }

    @Delete("/{followeeUserId}")
    @Status(HttpStatus.NO_CONTENT)
    public void unfollow(@NotNull UUID userId, @NotNull UUID followeeUserId)
    {
        commands.unfollow(userId, followeeUserId);
    }
}