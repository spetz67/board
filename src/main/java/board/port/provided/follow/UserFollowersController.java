package board.port.provided.follow;

import board.application.FollowQueryRepository;
import board.application.follow.FollowerOut;
import board.port.provided.ApiCommonResponses;
import board.port.provided.PageResource;
import board.port.provided.PageableParameter;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.RequestBean;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Controller("/api/users/{userId}/followers")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@ExecuteOn(TaskExecutors.IO)
@ApiCommonResponses
@Tag(name = "Following")
public class UserFollowersController
{
    private final FollowQueryRepository queries;

    @Get
    public PageResource<FollowerOut> getFollowers(@NotNull UUID userId,
            @RequestBean PageableParameter pageableParameter)
    {
        var page = queries.queryFollowersByUserId(userId, pageableParameter.toPageable());
        return new PageResource<>(page);
    }

    @Get("/{followerUserId}")
    public FollowerOut getFollowerByUserId(@NotNull UUID userId, UUID followerUserId)
    {
        return queries.queryFollowerByUserAndFollowerUserId(userId, followerUserId);
    }
}