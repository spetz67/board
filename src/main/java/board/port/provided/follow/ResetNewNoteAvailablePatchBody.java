package board.port.provided.follow;

import io.micronaut.core.annotation.Introspected;
import lombok.Data;

@Data
@Introspected
public class ResetNewNoteAvailablePatchBody
{
    private Boolean newNoteAvailable;
}
