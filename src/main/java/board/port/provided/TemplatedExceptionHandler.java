/*
 * Copyright 2017-2019 original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package board.port.provided;

import board.application.RevisionMismatchException;
import board.application.note.NoteNotFoundException;
import board.application.user.UserNotFoundException;
import board.domain.common.TemplatedException;
import io.micronaut.context.MessageSource;
import io.micronaut.http.*;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import jakarta.inject.Singleton;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

@Singleton
@RequiredArgsConstructor
@Produces(MediaType.APPLICATION_JSON)
@Slf4j
public class TemplatedExceptionHandler implements ExceptionHandler<TemplatedException, HttpResponse<JsonError>>
{
    private final static Map<Class<? extends Throwable>, HttpStatus> HTTP_STATUS_MAP = Map.of(
            UserNotFoundException.class, HttpStatus.NOT_FOUND,
            NoteNotFoundException.class, HttpStatus.NOT_FOUND,
            RevisionMismatchException.class, HttpStatus.CONFLICT
    );

    private final MessageSource messageSource;

    @Override
    public HttpResponse<JsonError> handle(HttpRequest request, TemplatedException e)
    {
        Locale locale = request.getHeaders()
                .findFirst(HttpHeaders.ACCEPT_LANGUAGE)
                .map(Locale::new)
                .orElse(Locale.getDefault());
        ResourceBundle res = ResourceBundle.getBundle("board.port.provided.ErrorRes", locale);
        String template = res.getString(e.getCode());
        String message = messageSource.interpolate(template, MessageSource.MessageContext.of(e.getVariables()));

        JsonError error = new JsonError(message)
                .path(request.getPath());

        HttpStatus httpStatus = HTTP_STATUS_MAP.getOrDefault(e.getClass(), HttpStatus.BAD_REQUEST);

        log.error("Error response {} ({}) on {} {}; {}",
                httpStatus.getCode(), httpStatus.getReason(), request.getMethod(), request.getUri(), message);

        return HttpResponse
                .ok(error)
                .status(httpStatus);
    }
}
