package board.port.required;

import org.jooq.Field;
import org.jooq.JSON;
import org.jooq.JSONArrayAggOrderByStep;
import org.jooq.tools.StringUtils;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.jooq.impl.DSL.*;

public class JooqUtils
{
    public static JSONArrayAggOrderByStep<JSON> jsonObjectArrayAgg(Field<?>... fields)
    {
        return jsonArrayAgg(
                jsonObject(Stream.of(fields)
                        .map(field -> jsonEntry(StringUtils.toCamelCaseLC(field.getName()), field))
                        .collect(Collectors.toList())));
    }
}
