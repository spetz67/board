package board.port.required;

import board.domain.common.EventService;
import io.micronaut.context.event.ApplicationEventPublisher;
import io.micronaut.core.annotation.NonNull;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;

@Singleton
@RequiredArgsConstructor
public class EventServiceImpl implements EventService
{
    private final ApplicationEventPublisher<Object> delegate;

    @Override
    public void publishEvent(@NonNull Object event)
    {
        delegate.publishEvent(event);
    }
}
