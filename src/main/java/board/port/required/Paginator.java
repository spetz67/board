package board.port.required;

import board.application.Page;
import board.application.Pageable;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.jooq.Record;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.util.List;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.field;

@RequiredArgsConstructor
@Singleton
public class Paginator
{
    private final DSLContext jooq;

    public <R extends Record> Page<R> queryPage(SelectConditionStep<R> query, Pageable pageable)
    {
        List<R> list = query
                .orderBy(getSortFields(pageable))
                .limit(pageable.size()>0 ? pageable.size() : Integer.MAX_VALUE)
                .offset(pageable.offset())
                .fetch();
        long totalSize = jooq.selectCount()
                .from(query)
                .fetchOptional(DSL.count())
                .orElseThrow();

        return new Page<>(list, pageable, totalSize);
    }

    public List<SortField<Object>> getSortFields(Pageable pageable)
    {
        return pageable.orderBy()
                .stream()
                .map(order -> field(order.property()).sort(order.ascending() ? SortOrder.ASC : SortOrder.DESC))
                .collect(Collectors.toList());
    }
}
