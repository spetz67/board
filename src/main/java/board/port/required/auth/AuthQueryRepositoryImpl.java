package board.port.required.auth;

import board.application.AuthQueryRepository;
import board.port.required.jooq.Tables;
import board.port.required.jooq.tables.UserEmailContact;
import io.micronaut.cache.annotation.CacheConfig;
import io.micronaut.cache.annotation.Cacheable;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import jakarta.inject.Singleton;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Singleton
@CacheConfig("user")
@RequiredArgsConstructor
class AuthQueryRepositoryImpl implements AuthQueryRepository
{
    private final DSLContext jooq;

    @Override
    @Transactional
    @Cacheable
    public Optional<UUID> findUserIdByEmail(String emailAddress)
    {
        return jooq.select(UserEmailContact.USER_EMAIL_CONTACT.USER_ID)
                .from(Tables.USER_EMAIL_CONTACT)
                .where(Tables.USER_EMAIL_CONTACT.EMAIL_ADDRESS.eq(emailAddress))
                .fetchOptional(UserEmailContact.USER_EMAIL_CONTACT.USER_ID);
    }

    @Override
    public boolean isAdmin(String emailAddress)
    {
        return "spetz67@googlemail.com".equals(emailAddress);
    }
}