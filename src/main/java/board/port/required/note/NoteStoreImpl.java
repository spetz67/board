package board.port.required.note;

import board.application.note.NoteNotFoundException;
import board.domain.note.Note;
import board.domain.note.NoteStore;
import board.port.required.jooq.tables.records.NoteRecord;
import io.micronaut.cache.annotation.CacheConfig;
import io.micronaut.cache.annotation.CacheInvalidate;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import jakarta.inject.Singleton;
import javax.transaction.Transactional;
import java.util.UUID;

import static board.port.required.jooq.tables.Note.NOTE;

@RequiredArgsConstructor
@Singleton
@CacheConfig("note")
class NoteStoreImpl extends NoteStore
{
    private final DSLContext jooq;

    @Override
    @Transactional
    protected int getNoteCountByUserId(UUID userId)
    {
        return jooq.fetchCount(NOTE, NOTE.USER_ID.eq(userId));
    }

    @Override
    protected Note getByUserIdAndNumber(UUID userId, int noteNumber)
    {
        return jooq.select(NOTE.USER_ID, NOTE.NUMBER, NOTE.INSTANT, NOTE.TEXT)
                .from(NOTE)
                .where(NOTE.USER_ID.eq(userId).and(NOTE.NUMBER.eq(noteNumber)))
                .fetchOptionalInto(Note.class)
                .orElseThrow(() -> new NoteNotFoundException(noteNumber));
    }

    @Override
    @CacheInvalidate(all = true)
    protected void save(Note note)
    {
        var id = note.userId()+":"+note.number();
        var record = new NoteRecord(id, note.userId(), note.number(), note.instant(), note.text());
        jooq.executeInsert(record);
    }

    @Override
    @CacheInvalidate(all = true)
    protected void delete(Note note)
    {
        jooq.deleteFrom(NOTE)
                .where(NOTE.USER_ID.eq(note.userId()).and(NOTE.NUMBER.eq(note.number())))
                .execute();
    }

    @Override
    @CacheInvalidate(all = true)
    protected void deleteAllByUserId(UUID userId)
    {
        jooq.deleteFrom(NOTE)
                .where(NOTE.USER_ID.eq(userId))
                .execute();
    }
}