package board.port.required.note;

import board.application.NoteQueryRepository;
import board.application.Page;
import board.application.Pageable;
import board.domain.note.Note;
import board.port.required.Paginator;
import io.micronaut.cache.annotation.CacheConfig;
import io.micronaut.cache.annotation.Cacheable;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import javax.transaction.Transactional;
import java.util.UUID;

import static board.port.required.jooq.tables.Note.NOTE;

@RequiredArgsConstructor
@Singleton
@CacheConfig("note")
@Cacheable
@Transactional
class NoteQueryRepositoryImpl implements NoteQueryRepository
{
    private final DSLContext jooq;
    private final Paginator paginator;
    private final NoteStoreImpl noteStore;

    @Transactional
    @Override
    public Page<Note> queryNotesByUserId(UUID userId, Integer fromNumber, Pageable pageable)
    {
        if (fromNumber==null)
            fromNumber = 1;

        if (!pageable.sorted())
            pageable = pageable.order("number");

        var query = jooq
                .select(NOTE.USER_ID, NOTE.NUMBER, NOTE.INSTANT, NOTE.TEXT)
                .from(NOTE)
                .where(NOTE.USER_ID.eq(userId).and(NOTE.NUMBER.greaterOrEqual(fromNumber)));
        return paginator
                .queryPage(query, pageable)
                .map(record -> record.into(Note.class));
    }

    @Transactional
    @Override
    public Note queryNoteByUserIdAndNumber(UUID userId, int noteNumber)
    {
        return noteStore.getByUserIdAndNumber(userId, noteNumber);
    }
}