package board.port.required.user;

import board.application.UserQueryRepository;
import board.application.user.OtherUserOut;
import board.application.user.UserNotFoundException;
import board.application.user.UserOut;
import board.port.required.Paginator;
import io.micronaut.cache.annotation.CacheConfig;
import io.micronaut.cache.annotation.Cacheable;
import board.application.Page;
import board.application.Pageable;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.JSON;
import org.jooq.Record;
import org.jooq.SelectJoinStep;

import jakarta.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static board.port.required.jooq.Tables.*;
import static org.jooq.impl.DSL.*;

@Singleton
@CacheConfig("user")
//@Cacheable
@RequiredArgsConstructor
class UserQueryRepositoryImpl implements UserQueryRepository
{
    private final DSLContext jooq;
    private final Paginator paginator;

    @Transactional
    @Override
    public Page<UserOut> queryUsers(Pageable pageable)
    {
        List<UserOut> users = selectFromUserWithEmailContactsAsJson()
                .orderBy(paginator.getSortFields(pageable))
                .offset(pageable.offset())
                .limit(pageable.size()>0 ? pageable.size() : Integer.MAX_VALUE)
                .fetchInto(UserOut.class);
        int totalSize = jooq.fetchCount(USER);
        return new Page<>(users, pageable, totalSize);
    }

    @Transactional
    @Override
    @Cacheable
    public UserOut queryUserById(UUID userId)
    {
        return selectFromUserWithEmailContactsAsJson()
                .where(USER.ID.equal(userId))
                .fetchOptionalInto(UserOut.class)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }

    @Override
    public Stream<OtherUserOut> queryOtherUsersByUserId(UUID userId, Pageable pageable)
    {
        return jooq.select(USER.ID, USER.NAME,
                field(notExists(jooq.select()
                        .from(FOLLOW_RELATION)
                        .where(FOLLOW_RELATION.FOLLOWEE_USER_ID.equal(USER.ID), FOLLOW_RELATION.FOLLOWER_USER_ID.equal(userId))))
                        .as("can_follow"))
                .from(USER)
                .where(USER.ID.notEqual(userId))
                .orderBy(paginator.getSortFields(pageable))
                .offset(pageable.offset())
                .limit(pageable.size()>0 ? pageable.size() : Integer.MAX_VALUE)
                .fetchStreamInto(OtherUserOut.class);
    }

    private SelectJoinStep<? extends Record> selectFromUserWithEmailContactsAsJson()
    {
        // select in same order as UserOut constructor args
        return jooq.select(USER.ID,
                        USER.UPDATED_INSTANT,
                        USER.TOTAL_WORD_COUNT,
                        jooq.selectCount()
                                .from(FOLLOW_RELATION)
                                .where(FOLLOW_RELATION.FOLLOWEE_USER_ID.equal(USER.ID))
                                .asField("follower_count"),
                        USER.NAME,
                        USER.PHONE_CONTACTS_JSON.as("phone_contacts"),
                        jooq.select(coalesce(jsonArrayAgg(
                                jsonObject(jsonEntry("type", USER_EMAIL_CONTACT.TYPE),
                                        jsonEntry("emailAddress", USER_EMAIL_CONTACT.EMAIL_ADDRESS))), JSON.json("[]")))
                                .from(USER_EMAIL_CONTACT)
                                .where(USER_EMAIL_CONTACT.USER_ID.equal(USER.ID))
                                .asField("email_contacts"))
                .from(USER);
    }
}