package board.port.required.user;

import board.application.RevisionMismatchException;
import board.application.user.UserNotFoundException;
import board.domain.user.User;
import board.domain.user.UserStore;
import board.port.required.jooq.tables.records.UserRecord;
import io.micronaut.cache.annotation.CacheConfig;
import io.micronaut.cache.annotation.CacheInvalidate;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import jakarta.inject.Singleton;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.UUID;
import java.util.stream.Collectors;

import static board.port.required.jooq.Tables.USER_EMAIL_CONTACT;
import static board.port.required.jooq.tables.User.USER;

@Singleton
@CacheConfig("user")
@RequiredArgsConstructor
class UserStoreImpl extends UserStore
{
    private final DSLContext jooq;
    private final EntityTranslator entityTranslator;

    @Override
    @Transactional
    protected User getById(UUID id)
    {
        return jooq.selectFrom(USER)
                .where(USER.ID.eq(id))
                .fetchOptional(record -> entityTranslator.toUser(record, jooq
                        .fetch(USER_EMAIL_CONTACT, USER_EMAIL_CONTACT.USER_ID.equal(id))
                        .map(entityTranslator::toEmailContact)))
                .orElseThrow(() -> new UserNotFoundException(id));

//        return jooq.select(USER.ID, USER.REVISION_NUMBER, USER.CREATED_INSTANT, USER.UPDATED_INSTANT,
//                USER.NAME, USER.TOTAL_WORD_COUNT,
//                USER.PHONE_CONTACTS_JSON.cast(JSON.class),
//                coalesce(jooq.select(JooqUtils.jsonObjectArrayAgg(USER_EMAIL_CONTACT.TYPE, USER_EMAIL_CONTACT.EMAIL_ADDRESS))
//                        .from(USER_EMAIL_CONTACT)
//                        .where(USER_EMAIL_CONTACT.USER_ID.equal(Tables.USER.ID)), array())
//                        .as("email_contacts"))
//                .from(USER)
//                .where(USER.ID.eq(id))
//                .fetchOptionalInto(User.class)
//                .orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    @Transactional
    @CacheInvalidate(all = true)
    protected User save(User user)
    {
        UserRecord record = entityTranslator.toNewRecord(user);
        jooq.insertInto(USER)
                .set(record)
                .execute();

        jooq.batchInsert(user.getEmailContacts().stream()
                .map(ec -> entityTranslator.toNewRecord(ec, user.getId()))
                .collect(Collectors.toList()))
                .execute();

        return entityTranslator.toUser(record, user.getEmailContacts());
    }

    @Override
    @Transactional
    @CacheInvalidate(all = true)
    protected void update(int newRevisionNumber, Instant updatedInstant, User user)
    {
        if (jooq.update(USER)
                .set(USER.REVISION_NUMBER, newRevisionNumber)
                .set(USER.UPDATED_INSTANT, updatedInstant)
                .set(USER.NAME, user.getName())
                .set(USER.TOTAL_WORD_COUNT, user.getTotalWordCount())
                .set(USER.PHONE_CONTACTS_JSON, entityTranslator.toJson(user.getPhoneContacts()))
                .where(USER.ID.eq(user.getId()), USER.REVISION_NUMBER.eq(user.getRevisionNumber()))
                .execute() == 0)
            throw new RevisionMismatchException(user.getRevisionNumber());

        var ecIterator = user.getEmailContacts().iterator();
        var recordIterator = jooq.fetch(USER_EMAIL_CONTACT, USER_EMAIL_CONTACT.USER_ID.equal(user.getId())).iterator();
        while (ecIterator.hasNext() || recordIterator.hasNext())
        {
            var ec = ecIterator.hasNext() ? ecIterator.next() : null;
            var record = recordIterator.hasNext() ? recordIterator.next() : null;

            if (ec!=null && record!=null && !ec.equals(entityTranslator.toEmailContact(record)))
            {
                record.setType(ec.type().name());
                record.setEmailAddress(ec.emailAddress());
                record.update();
            }

            if (ec==null && record!=null)
                record.delete();

            if (ec!=null && record==null)
                jooq.executeInsert(entityTranslator.toNewRecord(ec, user.getId()));
        }
    }

    @Override
    @Transactional
    @CacheInvalidate(all = true)
    protected boolean deleteById(UUID id)
    {
        boolean deleted = jooq.deleteFrom(USER)
                .where(USER.ID.eq(id))
                .execute() > 0;
        return deleted;
    }
}