package board.port.required.user;

import board.domain.user.EmailContact;
import board.domain.user.PhoneContact;
import board.domain.user.User;
import board.port.required.jooq.Tables;
import board.port.required.jooq.tables.records.UserEmailContactRecord;
import board.port.required.jooq.tables.records.UserRecord;
import io.micronaut.core.type.Argument;
import io.micronaut.http.codec.MediaTypeCodec;

import jakarta.inject.Named;
import jakarta.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Singleton
class EntityTranslator
{
    private final MediaTypeCodec jsonCodec;

    EntityTranslator(@Named("json") MediaTypeCodec jsonCodec)
    {
        this.jsonCodec = jsonCodec;
    }

    byte[] toJson(List<PhoneContact> phoneContacts)
    {
        return jsonCodec.encode(phoneContacts);
    }

    User toUser(UserRecord record, List<EmailContact> emailContacts)
    {
        return new User(record.getId(), record.getRevisionNumber(), record.getCreatedInstant(),
                record.getUpdatedInstant(), record.getName(), record.getTotalWordCount(),
                jsonCodec.decode(Argument.listOf(PhoneContact.class), record.getPhoneContactsJson()), emailContacts);
    }

    EmailContact toEmailContact(UserEmailContactRecord record)
    {
        return new EmailContact(EmailContact.Type.valueOf(record.getType()), record.getEmailAddress());
    }

    UserRecord toNewRecord(User user)
    {
        return new UserRecord(user.getId(), 1, user.getCreatedInstant(),
                user.getUpdatedInstant(), user.getName(), user.getTotalWordCount(),
                toJson(user.getPhoneContacts()));
    }

    UserEmailContactRecord toNewRecord(EmailContact ec, @NotNull UUID userId)
    {
        var record = new UserEmailContactRecord(null, userId, ec.type().name(), ec.emailAddress());
        record.reset(Tables.USER_EMAIL_CONTACT.ID);
        return record;
    }
}
