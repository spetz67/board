package board.port.required.follow;

import board.application.follow.FollowRelationNotFoundException;
import board.domain.follow.FollowRelation;
import board.domain.follow.FollowRelationRepository;
import io.micronaut.cache.annotation.CacheConfig;
import io.micronaut.cache.annotation.CacheInvalidate;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import jakarta.inject.Singleton;
import javax.transaction.Transactional;
import java.util.UUID;
import java.util.stream.Stream;

import static board.port.required.jooq.tables.FollowRelation.FOLLOW_RELATION;

@RequiredArgsConstructor
@Singleton
@CacheConfig(cacheNames = {FollowQueryRepositoryImpl.FOLLOWER_CACHE, FollowQueryRepositoryImpl.FOLLOWEE_CACHE})
class FollowRelationRepositoryImpl extends FollowRelationRepository
{
    private final DSLContext jooq;
    private final EntityTranslator entityTranslator;

    @Override
    @Transactional
    public FollowRelation get(UUID follower, UUID followee)
    {
        return jooq.selectFrom(FOLLOW_RELATION)
                .where(FOLLOW_RELATION.FOLLOWER_USER_ID.eq(follower), FOLLOW_RELATION.FOLLOWEE_USER_ID.eq(followee))
                .fetchOptional(entityTranslator::toFollowRelation)
                .orElseThrow(() -> new FollowRelationNotFoundException(follower, followee));
    }

    @Override
    @CacheInvalidate(all = true)
    @Transactional
    public FollowRelation add(FollowRelation followRelation)
    {
        jooq.insertInto(FOLLOW_RELATION)
                .set(entityTranslator.toRecord(followRelation))
                .execute();
        return followRelation;
    }

    @Override
    @CacheInvalidate(all = true)
    @Transactional
    public FollowRelation update(FollowRelation followRelation)
    {
        jooq.update(FOLLOW_RELATION)
                .set(entityTranslator.toRecord(followRelation))
                .where(FOLLOW_RELATION.FOLLOWER_USER_ID.eq(followRelation.getFollower().userId()),
                        FOLLOW_RELATION.FOLLOWEE_USER_ID.eq(followRelation.getFollowee().userId()))
                .execute();
        return followRelation;
    }

    @Override
    @CacheInvalidate(all = true)
    @Transactional
    public void delete(UUID follower, UUID followee)
    {
        jooq.deleteFrom(FOLLOW_RELATION)
                .where(FOLLOW_RELATION.FOLLOWER_USER_ID.eq(follower), FOLLOW_RELATION.FOLLOWEE_USER_ID.eq(followee))
                .execute();
    }

    @Override
    @Transactional
    protected Stream<FollowRelation> getByFollower(UUID follower)
    {
        return jooq.selectFrom(FOLLOW_RELATION)
                .where(FOLLOW_RELATION.FOLLOWER_USER_ID.eq(follower))
                .fetchStream()
                .map(entityTranslator::toFollowRelation);
    }

    @Override
    @Transactional
    protected Stream<FollowRelation> getByFollowee(UUID followee)
    {
        return jooq.selectFrom(FOLLOW_RELATION)
                .where(FOLLOW_RELATION.FOLLOWEE_USER_ID.eq(followee))
                .fetchStream()
                .map(entityTranslator::toFollowRelation);
    }
}
