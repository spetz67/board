package board.port.required.follow;

import board.domain.follow.FollowRelation;
import board.domain.follow.Followee;
import board.domain.follow.Follower;
import board.port.required.jooq.tables.records.FollowRelationRecord;
import jakarta.inject.Singleton;

import java.util.UUID;

@Singleton
class EntityTranslator
{
    FollowRelation toFollowRelation(FollowRelationRecord record)
    {
        return new FollowRelation(record.getCreatedInstant(),
                new Follower(record.getFollowerUserId(), record.getFollowerUserName()),
                new Followee(record.getFolloweeUserId(), record.getFolloweeUserName(),
                        record.getFolloweeFirstUnreadNoteNumber()));
    }

    FollowRelationRecord toRecord(FollowRelation followRelation)
    {
        Follower follower = followRelation.getFollower();
        Followee followee = followRelation.getFollowee();
        String id = computeId(follower.userId(), followee.userId());
        return new FollowRelationRecord(id, followRelation.getCreatedInstant(),
                follower.userId(), follower.userName(),
                followee.userId(), followee.userName(), followee.firstUnreadNoteNumber());
    }

    static String computeId(UUID follower, UUID followee)
    {
        return follower+":"+followee;
    }
}