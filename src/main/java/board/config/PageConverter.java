package board.config;

import io.micronaut.core.convert.ConversionContext;
import io.micronaut.core.convert.TypeConverter;
import board.application.Page;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import java.util.Optional;

/**
 * Workaround to enable caching of {@link Page} objects.
 * Without this they are not converted back from the cache.
 */
@Singleton
@RequiredArgsConstructor
public class PageConverter implements TypeConverter<Page<?>, Page<?>>
{
    @Override
    public Optional<Page<?>> convert(Page<?> object, Class<Page<?>> targetType, ConversionContext context)
    {
        return Optional.ofNullable(object);
    }
}
