package board.config;

import io.micronaut.context.annotation.Context;

import jakarta.annotation.PostConstruct;
import java.util.Locale;

@Context
public class LocaleSetup
{
    @PostConstruct
    public void setDefaultLocale()
    {
        Locale.setDefault(Locale.ENGLISH);
    }
}