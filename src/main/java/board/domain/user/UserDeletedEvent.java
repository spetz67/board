package board.domain.user;

import java.util.UUID;

public record UserDeletedEvent(UUID userId)
{
}
