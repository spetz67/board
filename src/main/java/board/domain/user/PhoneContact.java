package board.domain.user;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Introspected
public record PhoneContact(@NotNull Type type, @NotEmpty String number)
{
    public enum Type { PRIVATE, COMPANY, MOBILE }
}
