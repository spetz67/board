package board.domain.user;

public record UserNameChangedEvent(User user)
{
}
