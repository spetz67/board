package board.domain.user;

import board.domain.common.EventService;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import javax.transaction.Transactional;
import java.time.Clock;
import java.time.Instant;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class UserRepository
{
    private final UserStore userStore;
    private final EventService eventService;
    private final Clock clock;

    public User getById(UUID id)
    {
        return userStore.getById(id);
    }

    @Transactional
    public User add(User user)
    {
        user.setUpdatedInstant(clock.instant());
        User persistedUser = userStore.save(user);
        eventService.publishEvent(new UserAddedEvent(user));
//        publishEvents(user);
        user.getEventQueue().takeEvents();  // drop events created before saving
        return persistedUser;
    }

    @Transactional
    public void update(User user)
    {
        Instant now = clock.instant();
        int newRevisionNumber = user.getRevisionNumber()+1;
        userStore.update(newRevisionNumber, now, user);
        user.setRevisionNumber(newRevisionNumber);
        user.setUpdatedInstant(now);
        publishEvents(user);
    }

    @Transactional
    public void delete(UUID userId)
    {
        if (userStore.deleteById(userId))
            eventService.publishEvent(new UserDeletedEvent(userId));
    }

    private void publishEvents(User user)
    {
        for (Object event : user.getEventQueue().takeEvents())
            eventService.publishEvent(event);
    }
}
