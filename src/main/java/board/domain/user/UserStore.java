package board.domain.user;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

public abstract class UserStore
{
    protected abstract User getById(@NotNull UUID id);

    protected abstract User save(@Valid User user);

    protected abstract void update(int newRevisionNumber, Instant updatedInstant, @Valid User user);

    protected abstract boolean deleteById(@NotNull UUID userId);
}