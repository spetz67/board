package board.domain.user;

import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.time.Clock;
import java.util.List;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class UserFactory
{
    private final Clock clock;

    @Valid
    public User createUser(@NotEmpty String name)
    {
        return new User(UUID.randomUUID(), null, clock.instant(), null, name,
                0, List.of(), List.of());
    }
}
