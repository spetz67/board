package board.domain.user;

public final record UserAddedEvent(User user)
{
}
