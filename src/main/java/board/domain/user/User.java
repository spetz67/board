package board.domain.user;

import board.domain.common.EventQueue;
import io.micronaut.core.annotation.Introspected;
import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode
@ToString
@Introspected
public class User
{
    @Getter
    @EqualsAndHashCode.Exclude
    private final EventQueue eventQueue = new EventQueue();

    @Getter
    @NotNull
    private final UUID id;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    @Min(1)
    private Integer revisionNumber;

    @Getter
    @NotNull
    private final Instant createdInstant;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private Instant updatedInstant;

    @NotEmpty
    @Getter
    private String name;

    @Min(0)
    @Getter
    private int totalWordCount;

    @Getter
    @Setter
    @NotNull
    private List<PhoneContact> phoneContacts;

    @Getter
    @Setter
    @NotNull
    private List<EmailContact> emailContacts;


    public User(UUID id, Integer revisionNumber, Instant createdInstant, Instant updatedInstant, @NotEmpty String name,
            int totalWordCount, List<PhoneContact> phoneContacts, List<EmailContact> emailContacts)
    {
        this.id = id;
        this.revisionNumber = revisionNumber;
        this.createdInstant = createdInstant;
        this.updatedInstant = updatedInstant;
        this.name = name;
        this.totalWordCount = totalWordCount;
        this.phoneContacts = phoneContacts;
        this.emailContacts = emailContacts;
    }

    void addWordCount(int wordCount)
    {
        totalWordCount += wordCount;
    }

    void subtractWordCount(int wordCount)
    {
        totalWordCount -= wordCount;
    }

    public void setName(String name)
    {
        this.name = name;

        eventQueue.add(new UserNameChangedEvent(this));
    }
}
