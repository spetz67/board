package board.domain.user;

import board.domain.note.Note;
import board.domain.note.NoteDeletedEvent;
import board.domain.note.NotePostedEvent;
import io.micronaut.runtime.event.annotation.EventListener;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;

@Singleton
@RequiredArgsConstructor
public class NoteEventHandler
{
    private final UserRepository userRepository;

    @EventListener
    void notePosted(NotePostedEvent event)
    {
        Note note = event.note();
        User user = userRepository.getById(note.userId());
        user.addWordCount(note.computeWordCount());
        userRepository.update(user);
    }

    @EventListener
    void noteDeleted(NoteDeletedEvent event)
    {
        Note note = event.note();
        User user = userRepository.getById(note.userId());
        user.subtractWordCount(note.computeWordCount());
        userRepository.update(user);
    }
}
