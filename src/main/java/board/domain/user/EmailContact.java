package board.domain.user;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Introspected
public record EmailContact(@NotNull Type type, @NotEmpty String emailAddress)
{
    public enum Type { PRIVATE, COMPANY }
}
