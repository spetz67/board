package board.domain.common;

import lombok.Getter;

import java.util.Map;

@Getter
public class TemplatedException extends RuntimeException
{
    private String code;
    private Map<String, Object> variables;

    protected TemplatedException(String code, Map<String, Object> variables)
    {
        super(code+"; "+variables);

        this.code = code;
        this.variables = variables;
    }
}
