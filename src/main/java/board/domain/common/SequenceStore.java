package board.domain.common;

import java.util.UUID;

public interface SequenceStore
{
    int getNext(UUID id);

    void addSequence(UUID id);

    void deleteSequence(UUID id);
}