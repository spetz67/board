package board.domain.common;

import io.micronaut.context.event.ApplicationEventPublisher;

import io.micronaut.core.annotation.NonNull;

/**
 * Interface to publish domain events.
 *
 * This interface extends micronaut {@link ApplicationEventPublisher} just to enable navigation between event
 * publishers and listeners in IDEA.
 */
public interface EventService extends ApplicationEventPublisher
{
    void publishEvent(@NonNull Object event);
}
