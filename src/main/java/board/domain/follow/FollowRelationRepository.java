package board.domain.follow;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;
import java.util.stream.Stream;

public abstract class FollowRelationRepository
{
    public abstract FollowRelation get(@NotNull UUID follower, @NotNull UUID followee);

    public abstract FollowRelation add(@Valid FollowRelation followRelation);

    public abstract FollowRelation update(@Valid FollowRelation followRelation);

    public abstract void delete(@NotNull UUID follower, @NotNull UUID followee);

    protected abstract Stream<FollowRelation> getByFollowee(@NotNull UUID followee);

    protected abstract Stream<FollowRelation> getByFollower(@NotNull UUID follower);
}