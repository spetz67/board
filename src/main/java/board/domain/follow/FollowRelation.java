package board.domain.follow;

import board.domain.note.Note;
import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.validation.Valid;
import java.time.Instant;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Introspected
public class FollowRelation
{
    @Getter
    private final Instant createdInstant;

    @Getter
    @Valid
    private Follower follower;

    @Getter
    @Valid
    private Followee followee;

    public void resetFolloweeFirstUnreadNoteNumber()
    {
        if (followee.firstUnreadNoteNumber()!=null)
            followee = followee.withFirstUnreadNoteNumber(null);
    }

    void newNoteAvailableFromFollowee(Note note)
    {
        if (followee.firstUnreadNoteNumber()==null || followee.firstUnreadNoteNumber() > note.number())
            followee = followee.withFirstUnreadNoteNumber(note.number());
    }

    void setFollowerName(String followerUserName)
    {
        follower = follower.withUserName(followerUserName);
    }

    void setFolloweeName(String followeeUserName)
    {
        followee = followee.withUserName(followeeUserName);
    }
}
