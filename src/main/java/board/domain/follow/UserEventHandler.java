package board.domain.follow;

import board.domain.note.Note;
import board.domain.note.NotePostedEvent;
import board.domain.user.*;
import io.micronaut.runtime.event.annotation.EventListener;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
class UserEventHandler
{
    private final FollowRelationRepository followRelationRepository;

    @EventListener
    public void setNewNoteAvailableOnFollowees(NotePostedEvent event)
    {
        Note note = event.note();
        followRelationRepository
                .getByFollowee(note.userId())
                .forEach(followRelation -> {
                    followRelation.newNoteAvailableFromFollowee(note);
                    followRelationRepository.update(followRelation);
                });
    }

    @EventListener
    public void deleteRelatedFollowRelations(UserDeletedEvent event)
    {
        UUID userId = event.userId();

        followRelationRepository
                .getByFollower(userId)
                .forEach(followRelation -> followRelationRepository
                        .delete(followRelation.getFollower().userId(), followRelation.getFollowee().userId()));

        followRelationRepository
                .getByFollowee(userId)
                .forEach(followRelation -> followRelationRepository
                        .delete(followRelation.getFollower().userId(), followRelation.getFollowee().userId()));
    }

    @EventListener
    public void updateFollowRelationUserNames(UserNameChangedEvent event)
    {
        UUID userId = event.user().getId();

        followRelationRepository
                .getByFollower(userId)
                .forEach(followRelation -> {
                    followRelation.setFollowerName(event.user().getName());
                    followRelationRepository.update(followRelation);
                });

        followRelationRepository
                .getByFollowee(userId)
                .forEach(followRelation -> {
                    followRelation.setFolloweeName(event.user().getName());
                    followRelationRepository.update(followRelation);
                });
    }
}
