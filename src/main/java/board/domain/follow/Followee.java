package board.domain.follow;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.Nullable;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Introspected
public record Followee(@NotNull UUID userId, @NotNull String userName,
                       @Nullable Integer firstUnreadNoteNumber)
{
    public boolean isNewNoteAvailable()
    {
        return firstUnreadNoteNumber!=null;
    }

    public Followee withUserName(@NotNull String userName)
    {
        return new Followee(this.userId, userName, this.firstUnreadNoteNumber);
    }

    public Followee withFirstUnreadNoteNumber(Integer firstUnreadNoteNumber)
    {
        return new Followee(this.userId, this.userName, firstUnreadNoteNumber);
    }
}