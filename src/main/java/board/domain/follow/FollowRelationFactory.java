package board.domain.follow;

import board.domain.user.User;
import board.domain.user.UserRepository;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Clock;
import java.time.Instant;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class FollowRelationFactory
{
    private final Clock clock;
    private final UserRepository userRepository;

    @Valid
    public FollowRelation createFollowRelation(@NotNull UUID followerUserId, @NotNull UUID followeeUserId)
    {
        User followerUser = userRepository.getById(followerUserId);
        User followeeUser = userRepository.getById(followeeUserId);
        Follower follower = new Follower(followerUserId, followerUser.getName());
        Followee followee = new Followee(followeeUserId, followeeUser.getName(), null);
        return new FollowRelation(now(), follower, followee);
    }

    private Instant now()
    {
        return clock.instant();
    }
}
