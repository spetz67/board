package board.domain.follow;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Introspected
public record Follower(@NotNull UUID userId, @NotNull String userName)
{
    public Follower withUserName(@NotNull String userName)
    {
        return new Follower(this.userId, userName);
    }
}