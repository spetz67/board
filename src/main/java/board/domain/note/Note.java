package board.domain.note;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Introspected
public record Note(@NotNull @JsonIgnore UUID userId,
                   @Min(1) Integer number,
                   @NotNull Instant instant,
                   @NotEmpty String text)
{
    public int computeWordCount()
    {
        return text.split(" +").length;
    }
}
