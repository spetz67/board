package board.domain.note;

import board.domain.common.SequenceStore;
import board.domain.user.UserAddedEvent;
import board.domain.user.UserDeletedEvent;
import io.micronaut.runtime.event.annotation.EventListener;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Clock;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class NoteFactory
{
    private final Clock clock;
    private final SequenceStore sequenceStore;

    @Valid
    public Note createNote(@NotNull UUID userId, @NotEmpty String text)
    {
        int noteNumber = sequenceStore.getNext(userId);
        return new Note(userId, noteNumber, clock.instant(), text);
    }

    @EventListener
    void addSequence(UserAddedEvent event)
    {
        sequenceStore.addSequence(event.user().getId());
    }

    @EventListener
    void deleteSequence(UserDeletedEvent event)
    {
        sequenceStore.deleteSequence(event.userId());
    }
}
