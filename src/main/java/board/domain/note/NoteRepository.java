package board.domain.note;

import board.domain.common.EventService;
import board.domain.user.UserDeletedEvent;
import io.micronaut.runtime.event.annotation.EventListener;
import lombok.RequiredArgsConstructor;

import jakarta.inject.Singleton;
import javax.transaction.Transactional;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class NoteRepository
{
    private final NoteStore noteStore;
    private final EventService eventService;

    @Transactional
    public void add(Note note)
    {
        noteStore.save(note);
        eventService.publishEvent(new NotePostedEvent(note));
    }

    @Transactional
    public void delete(UUID userId, int noteNumber)
    {
        Note note = noteStore.getByUserIdAndNumber(userId, noteNumber);
        noteStore.delete(note);
        eventService.publishEvent(new NoteDeletedEvent(note));
    }

    public int getNoteCountByUserId(UUID userId)
    {
        return noteStore.getNoteCountByUserId(userId);
    }

    @EventListener
    void deleteUserNotes(UserDeletedEvent event)
    {
        noteStore.deleteAllByUserId(event.userId());
    }
}
