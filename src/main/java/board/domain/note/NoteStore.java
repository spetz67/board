package board.domain.note;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public abstract class NoteStore
{
    protected abstract Note getByUserIdAndNumber(@NotNull UUID userId, int number);

    protected abstract void save(@Valid Note note);

    protected abstract void delete(@NotNull Note note);

    protected abstract int getNoteCountByUserId(@NotNull UUID userId);

    protected abstract void deleteAllByUserId(@NotNull UUID userId);
}