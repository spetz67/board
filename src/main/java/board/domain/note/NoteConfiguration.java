package board.domain.note;

import io.micronaut.context.annotation.ConfigurationProperties;
import lombok.Data;

import jakarta.inject.Singleton;
import javax.validation.constraints.Min;

@Singleton
@Data
@ConfigurationProperties("note")
public class NoteConfiguration
{
    @Min(0)
    private Integer maxNotesPerUser;

    public boolean exceedsMaxNotes(int count)
    {
        return maxNotesPerUser!=null && count>maxNotesPerUser;
    }
}
