package board.domain.note;

public record NoteDeletedEvent(Note note)
{
}
