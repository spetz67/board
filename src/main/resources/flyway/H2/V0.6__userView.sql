-- User view for queries

-- alter table "user"
--     alter column "phone_contacts_json" json;

create view "user_view" as
select *,
       (select "COUNT"("id") from "follow_relation" where "followee_user_id" = "user_view"."id") as follower_count,
       (select "COALESCE"(
-- [jooq ignore start]
           "JSON_ARRAYAGG"("JSON_OBJECT"(key 'type' value "type", key 'emailAddress' value "email_address")),
-- [jooq ignore stop]
           "JSON_ARRAY"(null))
        from "user_email_contact"
        where "user_id" = "user_view"."id") email_contacts_json
from "user" "user_view"
