-- User email contacts
create table "user_email_contact"
(
    "id"      serial,
    "user_id" uuid                     not null,
    "type"    varchar(8)               not null,
    "email_address"    text            not null,
    primary key ("id"),
    foreign key ("user_id") references "user" ("id") on delete cascade
);

create index "user_email_contact_user_id" on "user_email_contact" ("user_id");
