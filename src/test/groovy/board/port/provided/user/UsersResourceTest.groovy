package board.port.provided.user

import board.application.user.UserIn
import board.domain.user.EmailContact
import board.domain.user.PhoneContact
import board.port.provided.AbstractResourceTest
import com.nimbusds.jose.PlainHeader
import com.nimbusds.jwt.PlainJWT
import com.nimbusds.jwt.SignedJWT
import groovy.transform.CompileStatic
import board.application.Pageable
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

@CompileStatic
class UsersResourceTest extends AbstractResourceTest
{
    @Test
    void createUser()
    {
        //language=json
        String request = """
        {
          "name":"test-user",
          "phoneContacts": [
            {
              "type": "PRIVATE",
              "number": "1234"
            }
          ],
          "emailContacts": [
            {
              "type": "PRIVATE",
              "emailAddress": "a@b.c"
            },
            {
              "type": "COMPANY",
              "emailAddress": "x@y.z"
            }
          ]
        }"""

        def response = restClient.post("/api/users", request)

        def createdUser = userQueries.queryUsers(Pageable.unpaged()).content().first()
        def userId = createdUser.id()

        assert response.status() == HttpStatus.CREATED
        assert response.header(HttpHeaders.CONTENT_TYPE) == null
        assert response.header(HttpHeaders.LOCATION) == "$baseUri/api/users/$userId"
        assert response.body() == null

        assert createdUser.name() == "test-user"
//        assert createdUser.phoneContacts[0].type == PhoneContact.Type.PRIVATE
//        assert createdUser.phoneContacts[0].number == "1234"
        JSONAssert.assertEquals('''[
            { "type": "PRIVATE", "number": "1234" } 
        ]''', createdUser.phoneContacts(), JSONCompareMode.STRICT)
//        assert createdUser.emailContacts[0].type == EmailContact.Type.PRIVATE
//        assert createdUser.emailContacts[0].emailAddress == "a@b.c"
//        assert createdUser.emailContacts[1].type == EmailContact.Type.MOBILE
//        assert createdUser.emailContacts[1].emailAddress == "x@y.z"
        JSONAssert.assertEquals('''[
            { "type": "PRIVATE", "emailAddress": "a@b.c" },
            { "type": "COMPANY", "emailAddress": "x@y.z" }
        ]''', createdUser.emailContacts(), JSONCompareMode.STRICT)
    }

    @Test
    void createUser_invalidName()
    {
        //language=json
        String request = """
        {
          "name":""
        }"""

        def response = restClient.exchange(HttpRequest.POST("/api/users", request))

        assert response.status() == HttpStatus.BAD_REQUEST
        assert response.header(HttpHeaders.CONTENT_TYPE) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""{
          "message": "Bad Request",
          "_links": {
            "self": {
              "href": "/api/users",
              "templated": false
            }
          },
          "_embedded": {
            "errors": [
              {
                "message": "userIn.name: must not be blank"
              }
            ]
          }
        }""", (String)response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void getUser()
    {
        def user1 = storeUser("user-1", new PhoneContact(PhoneContact.Type.PRIVATE, "1234"))
        postNote(user1, "message 1")
        postNote(user1, "message 2")

        restClient.setAuthToken(user1.id(), 'ROLE_USER')

        def response = restClient.get("/api/users/${user1.id()}")
        assert response.headers.getDate(HttpHeaders.LAST_MODIFIED).toInstant() == clock.instant()

        JSONAssert.assertEquals("""
        {
          "id": "${user1.id()}",
          "name": "user-1",
          "totalWordCount": 4,
          "followerCount": 0,
          "phoneContacts": [
            {
              "type": "PRIVATE",
              "number": "1234"
            }
          ],
          "emailContacts": []
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUser_notFound()
    {
        def id = UUID.randomUUID()

        def response = restClient.exchange(HttpRequest.GET("/api/users/$id"))

        assert response.status() == HttpStatus.NOT_FOUND
        assert response.header(HttpHeaders.CONTENT_TYPE) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "User not found by id: $id",
          "path": "/api/users/$id"
        }""", (String)response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUser_malformedId()
    {
        def response = restClient.exchange(HttpRequest.GET("/api/users/xxx"))

        assert response.status() == HttpStatus.BAD_REQUEST
        assert response.header(HttpHeaders.CONTENT_TYPE) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""{
          "message": "Bad Request",
          "_links": {
            "self": {
              "href": "/api/users/xxx",
              "templated": false
            }
          },
          "_embedded": {
            "errors": [
              {
                "message": "Failed to convert argument [id] for value [xxx] due to: Invalid UUID string: xxx",
                "path": "/id"
              }
            ]
          }
        }""", (String)response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void getUser_differentPrincipal()
    {
        def user1 = storeUser("user-1")
        def user2 = storeUser("user-2")

        restClient.setAuthToken(user2.id(), 'ROLE_USER')
        def response = restClient.exchange(HttpRequest.GET("/api/users/${user1.id()}"))

        assert response.status() == HttpStatus.FORBIDDEN
    }

    @Test
    void getUsers_0()
    {
        def response = restClient.get("/api/users")

        JSONAssert.assertEquals("""
        {
          "content": [],
          "number": 0,
          "size": 100,
          "totalElements": 0,
          "totalPages": 0
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUsers_1()
    {
        def user1 = storeUser("user-1")

        def response = restClient.get("/api/users")

        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "id": "${user1.id()}",
              "name": "user-1",
              "totalWordCount": 0,
              "followerCount": 0,
              "phoneContacts": [],
              "emailContacts": []
            }
          ],
          "number": 0,
          "size": 100,
          "totalElements": 1,
          "totalPages": 1
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUsers_2()
    {
        def user1 = storeUser("user-1")
        def user2 = storeUser("user-2")

        // run twice to ensure there is no conflict between stream result and caching
        (1..2).forEach {
            def response = restClient.get("/api/users?sort=name:desc")

            JSONAssert.assertEquals("""
            {
              "content": [
                {
                  "id": "${user2.id()}",
                  "name": "user-2",
                  "totalWordCount": 0,
                  "followerCount": 0,
                  "phoneContacts": [],
                  "emailContacts": []
                },
                {
                  "id": "${user1.id()}",
                  "name": "user-1",
                  "totalWordCount": 0,
                  "followerCount": 0,
                  "phoneContacts": [],
                  "emailContacts": []
                }
              ],
              "number": 0,
              "size": 100,
              "totalElements": 2,
              "totalPages": 1
            }""", response.body(), JSONCompareMode.STRICT)
        }
    }

    @Test
    void getUsers_page()
    {
        def users = (0..9).collect { new UserIn("user$it",
                null,
                List.of(new EmailContact(EmailContact.Type.PRIVATE, "u$it@1.mail"),
                        new EmailContact(EmailContact.Type.PRIVATE, "u$it@2.mail")))
        }
        def userOuts = users.reverse()
                .collect { userApp.registerUser(it) }
                .reverse()

        def response = restClient.get("/api/users?sort=name&size=3&page=1")

        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "id": "${userOuts[3].id()}",
              "name": "${users[3].name()}",
              "totalWordCount": 0,
              "followerCount": 0,
              "phoneContacts": [],
              "emailContacts": [
                {
                  "type": "${users[3].emailContacts()[0].type().name()}",
                  "emailAddress": "${users[3].emailContacts()[0].emailAddress()}"
                },
                {
                  "type": "${users[3].emailContacts()[1].type().name()}",
                  "emailAddress": "${users[3].emailContacts()[1].emailAddress()}"
                }
              ]
            },
            {
              "id": "${userOuts[4].id()}",
              "name": "${users[4].name()}",
              "totalWordCount": 0,
              "followerCount": 0,
              "phoneContacts": [],
              "emailContacts": [
                {
                  "type": "${users[4].emailContacts()[0].type().name()}",
                  "emailAddress": "${users[4].emailContacts()[0].emailAddress()}"
                },
                {
                  "type": "${users[4].emailContacts()[1].type().name()}",
                  "emailAddress": "${users[4].emailContacts()[1].emailAddress()}"
                }
              ]
            },
            {
              "id": "${userOuts[5].id()}",
              "name": "${users[5].name()}",
              "totalWordCount": 0,
              "followerCount": 0,
              "phoneContacts": [],
              "emailContacts": [
                {
                  "type": "${users[5].emailContacts()[0].type().name()}",
                  "emailAddress": "${users[5].emailContacts()[0].emailAddress()}"
                },
                {
                  "type": "${users[5].emailContacts()[1].type().name()}",
                  "emailAddress": "${users[5].emailContacts()[1].emailAddress()}"
                }
              ]
            }
          ],
          "number": 1,
          "size": 3,
          "totalElements": 10,
          "totalPages": 4
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUsers_unauthorized()
    {
        restClient.clearAuthToken()
        def response = restClient.exchange(HttpRequest.GET("/api/users"))

        assert response.status() == HttpStatus.UNAUTHORIZED
    }

    @Test
    void getUsers_notPermitted()
    {
        restClient.setAuthToken(UUID.randomUUID(), 'ROLE_USER')
        def response = restClient.exchange(HttpRequest.GET("/api/users"))

        assert response.status() == HttpStatus.FORBIDDEN
    }

    @Test
    void getUsers_unsignedToken()
    {
        def jwt = SignedJWT.parse(restClient.authToken.toString())
        def header = jwt.header.toJSONObject()
        header.put('alg', 'none')
        def plainJWT = new PlainJWT(PlainHeader.parse(header), jwt.JWTClaimsSet)
        restClient.authToken = plainJWT.serialize()

        def response = restClient.exchange(HttpRequest.GET("/api/users"))

        assert response.status() == HttpStatus.UNAUTHORIZED
    }
}
