package board.port.provided.user

import board.port.provided.AbstractResourceTest
import groovy.transform.CompileStatic
import io.micronaut.context.annotation.Property
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

@Property(name = "note.max-notes-per-user", value = "5")
@CompileStatic
class UserNotesResourceTest extends AbstractResourceTest
{
    @Test
    void getNotes_2()
    {
        def user1 = storeUser("user-1")
        (1..2).forEach { postNote(user1, "message $it") }

        // notes must be readable by other users
        def user2 = storeUser("user-2")
        restClient.setAuthToken(user2.id(), 'ROLE_USER')

        def response = restClient.get("/api/users/${user1.id()}/notes")

        def now = clock.instant()
        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "instant": "$now",
              "text": "message 1",
              "number": 1
            },
            {
              "instant": "$now",
              "text": "message 2",
              "number": 2
            }
          ],
          "number": 0,
          "size": 100,
          "totalElements": 2,
          "totalPages": 1
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getNotes_fromNumber()
    {
        def user1 = storeUser("user-1")
        (1..5).forEach { postNote(user1, "message $it") }

        def response = restClient.get("/api/users/${user1.id()}/notes?fromNumber=4&page=0")

        def now = clock.instant()
        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "instant": "$now",
              "text": "message 4",
              "number": 4
            },
            {
              "instant": "$now",
              "text": "message 5",
              "number": 5
            }
          ],
          "number": 0,
          "size": 100,
          "totalElements": 2,
          "totalPages": 1
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getNote()
    {
        def user1 = storeUser("user-1")
        (1..2).forEach { postNote(user1, "message $it") }

        def response = restClient.get("/api/users/${user1.id()}/notes/1")

        def now = clock.instant()
        JSONAssert.assertEquals("""
        {
          "instant": "$now",
          "text": "message 1",
          "number": 1
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void deleteNote()
    {
        def user1 = storeUser("user-1")
        (1..2).forEach { postNote(user1, "message $it") }

        def notesUri = "/api/users/${user1.id()}/notes"

        // check note is present
        restClient.get("$notesUri/1")

        def response = restClient.delete("$notesUri/1")

        assert response.status()==HttpStatus.NO_CONTENT
        assert restClient.exchange(HttpRequest.GET("$notesUri/1")).status == HttpStatus.NOT_FOUND
    }

    @Test
    void postNote()
    {
        def request = """
        {
          "text": "message 1"
        }"""

        def user1 = storeUser("user-1")
        def notesUri = "/api/users/${user1.id()}/notes"

        restClient.setAuthToken(user1.id(), 'ROLE_USER')

        def response = restClient.post(notesUri, request)

        assert response.status() == HttpStatus.CREATED
        assert response.header(HttpHeaders.CONTENT_TYPE) == null
        assert response.header(HttpHeaders.LOCATION) == "$baseUri$notesUri/1"
        assert response.body() == null
    }

    @Test
    void postNote_limitExceeded()
    {
        def request = """
        {
          "text": "message 1"
        }"""

        def user1 = storeUser("user-1")
        def notesUri = "/api/users/${user1.id()}/notes"
        (1..5).forEach { restClient.post(notesUri, request) }

        def response = restClient.exchange(HttpRequest.POST(notesUri, request))

        assert response.status() == HttpStatus.BAD_REQUEST
        assert response.header(HttpHeaders.CONTENT_TYPE) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "Number of notes limit of 5 exceeded",
          "path": "/api/users/${user1.id()}/notes"
        }""", (String)response.body(), JSONCompareMode.STRICT)
    }
}
