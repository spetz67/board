package board.port.provided.user

import board.port.provided.AbstractResourceTest
import groovy.transform.CompileStatic
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

@CompileStatic
class UserOthersResourceTest extends AbstractResourceTest
{
    @Test
    void getOthers_2()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        def user3 = storeUser("user3")
        follow(user1, user2.id())

        restClient.setAuthToken(user1.id(), 'ROLE_USER')

        // run twice to ensure there is no conflict between stream result and caching
        (1..2).forEach {
            def response = restClient.get("/api/users/${user1.id()}/others?sort=name")

            JSONAssert.assertEquals("""
            [
                {
                  "id": "${user2.id()}",
                  "name": "user2",
                  "canFollow": false
                },
                {
                  "id": "${user3.id()}",
                  "name": "user3",
                  "canFollow": true
                }
            ]""", response.body(), JSONCompareMode.STRICT)
        }
    }
}
