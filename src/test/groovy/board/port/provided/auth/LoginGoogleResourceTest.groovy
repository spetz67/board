package board.port.provided.auth

import board.application.user.UserIn
import board.domain.user.EmailContact
import board.port.provided.AbstractResourceTest
import com.nimbusds.jwt.JWTParser
import com.nimbusds.jwt.SignedJWT
import groovy.transform.CompileStatic
import io.micronaut.context.annotation.Replaces
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

import jakarta.inject.Inject
import jakarta.inject.Singleton

@MicronautTest(transactional = false, environments = "test")
@CompileStatic
class LoginGoogleResourceTest extends AbstractResourceTest
{
    @Inject
    GoogleLoginVerifier googleLoginVerifier

    @Singleton
    @Replaces(GoogleLoginVerifier)
    GoogleLoginVerifier googleLoginVerifier()
    {
        return Mockito.mock(GoogleLoginVerifier)
    }

    @Test
    void loginGoogle() {
        def user1 = userApp.registerUser(new UserIn("test-user",
                null,
                List.of(new EmailContact(EmailContact.Type.PRIVATE, "test-user@test-host"))))

        Mockito.when(googleLoginVerifier.getEmailAddress("test-token"))
                .thenReturn("test-user@test-host")

        def response = restClient.exchange(HttpRequest
                .POST("/login-google", "test-token")
                .contentType(MediaType.TEXT_PLAIN_TYPE))

        assert response.status() == HttpStatus.OK
        assert response.header(HttpHeaders.CONTENT_TYPE) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
              {
                "username": "${user1.id()}",
                "roles": [ "ROLE_USER" ],
                "token_type": "Bearer",
                "expires_in": 3600
              }""", response.body(), JSONCompareMode.LENIENT)

        def accessToken = new JSONObject(response.body()).getString("access_token")
        def jwt = JWTParser.parse(accessToken)
        assert jwt instanceof SignedJWT
        assert jwt.JWTClaimsSet.subject == user1.id().toString()
        assert jwt.JWTClaimsSet.getStringListClaim('roles') == ['ROLE_USER']
    }

    @Test
    void loginGoogle_admin()
    {
        Mockito.when(googleLoginVerifier.getEmailAddress("test-token"))
                .thenReturn("spetz67@googlemail.com")

        def response = restClient.exchange(HttpRequest
                .POST("/login-google", "test-token")
                .contentType(MediaType.TEXT_PLAIN_TYPE))

        assert response.status() == HttpStatus.OK
        assert response.header(HttpHeaders.CONTENT_TYPE) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
              {
                "username": "spetz67@googlemail.com",
                "roles": [ "ROLE_USER", "ROLE_ADMIN" ],
                "token_type": "Bearer",
                "expires_in": 3600
              }""", response.body(), JSONCompareMode.LENIENT)
        Assertions.assertNotNull(new JSONObject(response.body()).get("access_token"))
    }

    @Test
    void loginGoogle_unknown()
    {
        Mockito.when(googleLoginVerifier.getEmailAddress("test-token"))
                .thenReturn("test-user@test-host")

        def response = restClient.exchange(HttpRequest
                .POST("/login-google", "test-token")
                .contentType(MediaType.TEXT_PLAIN_TYPE))

        assert response.status() == HttpStatus.BAD_REQUEST
        assert response.header(HttpHeaders.CONTENT_TYPE) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "Login denied: Invalid credentials",
          "path": "/login-google"
        }""", (String)response.body(), JSONCompareMode.LENIENT)
    }
}
