package board.port.provided.follow

import board.port.provided.AbstractResourceTest
import groovy.transform.CompileStatic
import io.micronaut.http.HttpHeaders
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

@CompileStatic
class UserFollowerResourceTest extends AbstractResourceTest
{
    @Test
    void getFollowers_2()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        def user3 = storeUser("user3")
        follow(user2, user1.id())
        follow(user3, user1.id())

        restClient.setAuthToken(user1.id(), 'ROLE_USER')

        def response = restClient.get("/api/users/${user1.id()}/followers")

        def now = clock.instant()
        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "userId": "${user2.id()}",
              "createdInstant": "$now",
              "userName": "user2"
            },
            {
              "userId": "${user3.id()}",
              "createdInstant": "$now",
              "userName": "user3"
            }
          ],
          "number": 0,
          "size": 100,
          "totalElements": 2,
          "totalPages": 1
        }""", response.body(), JSONCompareMode.NON_EXTENSIBLE)
    }

    @Test
    void getFollower()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        def user3 = storeUser("user3")
        follow(user2, user1.id())
        follow(user3, user1.id())

        def response = restClient.get("/api/users/${user1.id()}/followers/${user2.id()}")

        def now = clock.instant()
        JSONAssert.assertEquals("""
        {
            "userId": "${user2.id()}",
            "createdInstant": "$now",
            "userName": "user2"
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUser()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        def user3 = storeUser("user3")
        follow(user2, user1.id())
        follow(user3, user1.id())

        def response = restClient.get("/api/users/${user1.id()}")
        assert response.headers.getDate(HttpHeaders.LAST_MODIFIED).toInstant() == clock.instant()

        // verify followerCount
        JSONAssert.assertEquals("""
        {
          "id": "${user1.id()}",
          "name": "user1",
          "totalWordCount": 0,
          "followerCount": 2,
          "phoneContacts": [],
          "emailContacts": []
        }""", response.body(), JSONCompareMode.STRICT)
    }
}
