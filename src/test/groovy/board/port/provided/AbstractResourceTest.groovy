package board.port.provided

import board.application.FollowCommands
import board.application.NoteCommands
import board.application.UserCommands
import board.application.UserQueryRepository
import board.application.note.NoteIn
import board.application.user.UserIn
import board.application.user.UserOut
import board.domain.user.PhoneContact
import board.port.required.TestRepository
import groovy.transform.CompileStatic
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import io.micronaut.transaction.SynchronousTransactionManager
import org.junit.jupiter.api.BeforeEach

import jakarta.inject.Inject
import java.time.Clock
import java.util.function.Supplier

@MicronautTest(transactional = false, environments = "test")
@CompileStatic
abstract class AbstractResourceTest
{
    @Inject
    RestClient restClient

    @Inject
    Clock clock

    @Inject
    SynchronousTransactionManager transactions

    @Inject
    TestRepository testRepository

    @Inject
    UserQueryRepository userQueries

    @Inject
    UserCommands userApp

    @Inject
    NoteCommands noteApp

    @Inject
    FollowCommands followApp

    def baseUri

    @BeforeEach
    void setup(EmbeddedServer embeddedServer)
    {
        baseUri = embeddedServer.getURI()

        restClient.setAuthToken(UUID.randomUUID(), 'ROLE_USER', 'ROLE_ADMIN')

        testRepository.deleteAll()
    }

    UserOut storeUser(String name, PhoneContact... phoneContacts) {
        userApp.registerUser(new UserIn(name, List.of(phoneContacts), null))
    }

    void postNote(UserOut user, String text)
    {
        noteApp.postNote(user.id(), new NoteIn(text))
    }

    void follow(UserOut user, UUID followeeUserId)
    {
        followApp.follow(user.id(), followeeUserId)
    }

    def <T> T transactional(Supplier<T> supplier) {

        return transactions.<T>executeRead {supplier.get() }
    }
}
