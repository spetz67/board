package board.port.provided

import groovy.transform.CompileStatic
import io.micronaut.core.type.Argument
import io.micronaut.http.*
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.token.jwt.generator.AccessRefreshTokenGenerator

import jakarta.inject.Inject
import jakarta.inject.Singleton

@Singleton
@CompileStatic
class RestClient
{
    @Inject
    @Client("/")
    HttpClient httpClient

    @Inject
    AccessRefreshTokenGenerator tokenGenerator;

    CharSequence authToken

    void setAuthToken(UUID userId, String... roles) {

        authToken = tokenGenerator.generate(Authentication.build(userId.toString(), List.of(roles)))
                .orElseThrow()
                .accessToken
    }

    void clearAuthToken() {

        authToken = null
    }

    HttpResponse<String> exchange(MutableHttpRequest<? extends Object> request)
    {
        return httpClient.toBlocking().exchange(prepare(request), Argument.STRING, Argument.STRING)
    }

    HttpResponse<String> get(String uri, String contentType=MediaType.APPLICATION_JSON)
    {
        def response = httpClient.toBlocking().exchange(
                prepare(HttpRequest.GET(uri)),
                String)

        assert response.status() == HttpStatus.OK
        assert response.header(HttpHeaders.CONTENT_TYPE) == contentType

        return response
    }

    HttpResponse<String> post(String uri, String request)
    {
        def response = httpClient.toBlocking().exchange(
                prepare(HttpRequest.POST(uri, request)),
                String)

        assert response.status() == HttpStatus.CREATED
        assert response.header(HttpHeaders.LOCATION) != null

        return response
    }

    HttpResponse<String> patch(String uri, String request) {

        def response = httpClient.toBlocking().exchange(
                prepare(HttpRequest.PATCH(uri, request)),
                String)

        assert response.status() == HttpStatus.NO_CONTENT

        return response
    }

    HttpResponse<String> delete(String uri) {

        def response = httpClient.toBlocking().exchange(
                prepare(HttpRequest.DELETE(uri)),
                String)

        assert response.status() == HttpStatus.NO_CONTENT
        assert response.header(HttpHeaders.CONTENT_TYPE) == null
        assert response.body() == null

        return response
    }

    private <T> MutableHttpRequest<T> prepare(MutableHttpRequest<T> request) {

        if (authToken)
            request = request.bearerAuth(authToken)

        return request
    }
}
