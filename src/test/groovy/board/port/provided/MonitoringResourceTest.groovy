package board.port.provided

import groovy.transform.CompileStatic
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

@MicronautTest(transactional = false, environments = "test")
@CompileStatic
class MonitoringResourceTest extends AbstractResourceTest
{
    @Test
    void info()
    {
        def response = restClient.get("/monitoring/info")

        assert response.status() == HttpStatus.OK
        assert response.header(HttpHeaders.CONTENT_TYPE) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals('''
              {
                "name": "board"
              }''', response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void info_unauthorized()
    {
        restClient.clearAuthToken()
        def response = restClient.exchange(HttpRequest.GET("/monitoring/info"))

        assert response.status() == HttpStatus.UNAUTHORIZED
    }

    @Test
    void info_notPermitted()
    {
        restClient.setAuthToken(UUID.randomUUID(), 'ROLE_USER')
        def response = restClient.exchange(HttpRequest.GET("/monitoring/info"))

        assert response.status() == HttpStatus.FORBIDDEN
    }
}
