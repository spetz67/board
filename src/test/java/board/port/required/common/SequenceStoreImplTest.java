package board.port.required.common;

import board.domain.common.SequenceStore;
import io.micronaut.context.annotation.Value;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@MicronautTest(transactional = false)
class SequenceStoreImplTest
{
    @Inject
    private SequenceStore sequenceStore;

    @Value("${sequence.test.query-count:10000}")
    private int queryCount;

    @RepeatedTest(6)
    void getNext_single()
    {
        UUID uuid = UUID.randomUUID();
        sequenceStore.addSequence(uuid);

        for (int i=1; i<=queryCount; i++)
            Assertions.assertEquals(i, sequenceStore.getNext(uuid));
    }

    @RepeatedTest(6)
    void getNext_multiple_parallel() throws InterruptedException, ExecutionException
    {
        int sequenceCount = 4;

        List<UUID> uuids = Stream.generate(UUID::randomUUID)
                .limit(sequenceCount)
                .collect(Collectors.toList());
        uuids.forEach(sequenceStore::addSequence);

        List<Callable<Object>> callables = uuids.stream()
                .map(uuid -> (Callable<Object>) () ->
                {
                    for (int i = 1; i<=queryCount; i++)
                        Assertions.assertEquals(i, sequenceStore.getNext(uuid));

                    return null;
                })
                .collect(Collectors.toList());
        ExecutorService threadPool = Executors.newFixedThreadPool(sequenceCount);
        for (Future<Object> future : threadPool.invokeAll(callables))
            future.get();
    }

    @RepeatedTest(6)
    void getNext_single_parallel() throws InterruptedException, ExecutionException
    {
        UUID uuid = UUID.randomUUID();
        sequenceStore.addSequence(uuid);
        Set<Integer> numbers = new ConcurrentSkipListSet<>();

        ExecutorService threadPool = Executors.newFixedThreadPool(2);
        for (Future<Object> objectFuture : threadPool.invokeAll(
                Collections.nCopies(queryCount, () -> numbers.add(sequenceStore.getNext(uuid)))))
            objectFuture.get();

        Assertions.assertEquals(queryCount, numbers.size());

        int index = 1;
        for (Integer number : numbers)
            Assertions.assertEquals(index++, number);
    }
}