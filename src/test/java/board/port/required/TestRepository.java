package board.port.required;

import io.micronaut.cache.annotation.CacheInvalidate;
import org.jooq.DSLContext;
import org.jooq.Table;

import jakarta.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;

import static board.port.required.jooq.Tables.*;

@Singleton
public class TestRepository
{
    private final DSLContext jooq;

    TestRepository(DSLContext jooq)
    {
        this.jooq = jooq;
    }

    @CacheInvalidate(all = true, cacheNames = {"user", "note", "follower", "followee"})
    @Transactional
    public void deleteAll()
    {
        for (Table<?> table: List.of(SEQUENCE, NOTE, FOLLOW_RELATION, USER, USER_EMAIL_CONTACT))
            jooq.deleteFrom(table).execute();
    }
}