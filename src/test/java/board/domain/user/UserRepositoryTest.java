package board.domain.user;

import board.application.NoteQueryRepository;
import board.application.RevisionMismatchException;
import board.application.user.UserNotFoundException;
import board.domain.common.TestEventReceiver;
import board.domain.note.NotePostedEvent;
import board.port.required.TestRepository;
import board.application.Pageable;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import java.util.List;

@MicronautTest(transactional = false, environments = "test")
public class UserRepositoryTest
{
    @Inject
    private UserRepository userRepository;

    @Inject
    private NoteQueryRepository noteQueries;

    @Inject
    private UserFactory userFactory;

    @Inject
    private TestRepository testRepository;

    @Inject
    private NotePostedEventReceiver notePostedEventReceiver;

    @Singleton
    static class NotePostedEventReceiver extends TestEventReceiver<NotePostedEvent> { }

    @AfterEach
    void tearDown()
    {
        testRepository.deleteAll();
    }

    @Test
    void save()
    {
        notePostedEventReceiver.reset();

        User initialUser = user("test-user");
        User user = userRepository.add(initialUser);

        Assertions.assertNotNull(user.getUpdatedInstant());
        Assertions.assertEquals(1, user.getRevisionNumber());

        User storedUser = userRepository.getById(user.getId());
        Assertions.assertEquals(user, storedUser);

        Assertions.assertThrows(org.jooq.exception.DataAccessException.class, () -> userRepository.add(user));
    }

    @Test
    void save_existing()
    {
        User user = userRepository.add(user("test-user"));
        User storedUser = userRepository.getById(user.getId());

        Assertions.assertThrows(org.jooq.exception.DataAccessException.class, () -> userRepository.add(storedUser));
    }

    @Test
    void update()
    {
        User initialUser = user("test-user");
        User user = userRepository.add(initialUser);
        notePostedEventReceiver.reset();

        user.setName("new-name");
        user.setPhoneContacts(List.of(
                new PhoneContact(PhoneContact.Type.COMPANY, "0123"),
                new PhoneContact(PhoneContact.Type.PRIVATE, "0125")
        ));
        user.setEmailContacts(List.of(
                new EmailContact(EmailContact.Type.COMPANY, "a@b.c"),
                new EmailContact(EmailContact.Type.PRIVATE, "m1@n.o")
        ));

        userRepository.update(user);

        User storedUser = userRepository.getById(user.getId());
        Assertions.assertEquals(user, storedUser);
    }

    @Test
    void delete()
    {
        User user = userRepository.add(user("test-user"));

        userRepository.delete(user.getId());

        Assertions.assertThrows(UserNotFoundException.class, () -> userRepository.getById(user.getId()));
        Assertions.assertEquals(0,
                noteQueries.queryNotesByUserId(user.getId(), null, Pageable.unpaged()).totalSize());

        userRepository.delete(user.getId());    // repeated call must succeed
    }

    @Test
    void update_revisionMismatch()
    {
        User user = userRepository.add(user("test-user"));
        User storedUser = userRepository.getById(user.getId());

        userRepository.update(user);
        Assertions.assertThrows(RevisionMismatchException.class, () -> userRepository.update(storedUser));
    }

    @Test
    void getUserById()
    {
        User user = user("test-user");
        userRepository.add(user);

        User storedUser = userRepository.getById(user.getId());
        Assertions.assertEquals(user.getId(), storedUser.getId());
        Assertions.assertEquals(1, storedUser.getRevisionNumber());
    }

    private User user(String name)
    {
        User user = userFactory.createUser(name);
        user.setPhoneContacts(List.of(
                new PhoneContact(PhoneContact.Type.COMPANY, "0123"),
                new PhoneContact(PhoneContact.Type.MOBILE, "0124")
        ));
        user.setEmailContacts(List.of(
                new EmailContact(EmailContact.Type.COMPANY, "a@b.c"),
                new EmailContact(EmailContact.Type.PRIVATE, "x@y.z")
        ));
        return user;
    }
}
