package board.domain.common;

import io.micronaut.context.event.ApplicationEventListener;

import java.util.ArrayList;
import java.util.List;

public class TestEventReceiver<E> implements ApplicationEventListener<E>
{
    private List<E> events = new ArrayList<>();

    public void reset()
    {
        events.clear();
    }

    public List<E> getEvents()
    {
        return events;
    }

    @Override
    public void onApplicationEvent(E event)
    {
        events.add(event);
    }

    @Override
    public boolean supports(E event)
    {
        return true;
    }
}
