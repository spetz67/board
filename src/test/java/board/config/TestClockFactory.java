package board.config;

import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Replaces;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoField;

@Factory
public class TestClockFactory
{
    private Clock clock = Clock.fixed(Instant.now().with(ChronoField.MILLI_OF_SECOND, 0), ZoneId.systemDefault());

    @Bean
    @Replaces(bean = Clock.class)
    public Clock clock()
    {
        return clock;
    }
}