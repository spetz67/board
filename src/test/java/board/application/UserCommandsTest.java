package board.application;

import board.application.user.UserIn;
import board.application.user.UserOut;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import jakarta.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;

@MicronautTest(transactional = false, environments = "test")
class UserCommandsTest
{
    @Inject
    private UserCommands userCommands;

    @Inject
    private UserQueryRepository userQueries;

    @Inject
    private FollowCommands followCommands;

    @Inject
    private FollowQueryRepository followQueries;

    @Test
    void updateUser()
    {
        UserOut user1 = userCommands.registerUser(new UserIn("user1", null, null));
        UserOut user2 = userCommands.registerUser(new UserIn("user2", null, null));
        UserOut user3 = userCommands.registerUser(new UserIn("user3", null, null));
        UserOut user4 = userCommands.registerUser(new UserIn("user4", null, null));
        followCommands.follow(user1.id(), user2.id());
        followCommands.follow(user1.id(), user3.id());
        followCommands.follow(user4.id(), user1.id());

        userCommands.updateUser(user1.id(), new UserIn("new-name", null, null));

        UserOut storedUser = userQueries.queryUserById(user1.id());

        assertEquals("new-name", storedUser.name());

        assertEquals(storedUser.name(),
                followQueries.queryFollowerByUserAndFollowerUserId(user2.id(), user1.id()).userName());
        assertEquals(storedUser.name(),
                followQueries.queryFollowerByUserAndFollowerUserId(user3.id(), user1.id()).userName());
        assertEquals(storedUser.name(),
                followQueries.queryFolloweeByUserAndFolloweeUserId(user4.id(), user1.id()).userName());
    }
}