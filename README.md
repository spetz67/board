# Microservice demo project


## Requirements


### Long term maintainability

The project is strictly following DDD principles.

All domain functionality is explicitly coded, e.g.:

- entity created timestamps are set in domain factories
- entity updated timestamps are set in domain repositories
- domain events are published in domain repositories
- functionality provided by frameworks is modeled as domain service interfaces and implemented in adapter classes,
  e.g. EventService, TimeService

JDO classes are generated by jooq from DDL to ensure classes always match

Flyway is used to ensure the database always matches the data model defined by DDL


### Testing

- integration tests against H2 database in postgres mode and against postgres running in docker container


### Suitability for high amounts of data records


## Micronaut 2.5.4 Documentation

- [User Guide](https://docs.micronaut.io/2.5.4/guide/index.html)
- [API Reference](https://docs.micronaut.io/2.5.4/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/2.5.4/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
---

### Feature jdbc-hikari documentation

- [Micronaut Hikari JDBC Connection Pool documentation](https://micronaut-projects.github.io/micronaut-sql/latest/guide/index.html#jdbc)

### Feature cache-caffeine documentation

- [Micronaut Caffeine Cache documentation](https://micronaut-projects.github.io/micronaut-cache/latest/guide/index.html)

- [https://github.com/ben-manes/caffeine](https://github.com/ben-manes/caffeine)

### Feature mockito documentation

- [https://site.mockito.org](https://site.mockito.org)

### Feature data-jdbc documentation

- [Micronaut Data JDBC documentation](https://micronaut-projects.github.io/micronaut-data/latest/guide/index.html#jdbc)

### Feature jms-activemq-artemis documentation

- [Micronaut ActiveMQ Artemis JMS Messaging documentation](https://micronaut-projects.github.io/micronaut-jms/snapshot/guide/index.html)

### Feature lombok documentation

- [Micronaut Project Lombok documentation](https://docs.micronaut.io/latest/guide/index.html#lombok)

- [https://projectlombok.org/features/all](https://projectlombok.org/features/all)

### Feature flyway documentation

- [Micronaut Flyway Database Migration documentation](https://micronaut-projects.github.io/micronaut-flyway/latest/guide/index.html)

- [https://flywaydb.org/](https://flywaydb.org/)

### Feature management documentation

- [Micronaut Management documentation](https://docs.micronaut.io/latest/guide/index.html#management)

### Feature http-client documentation

- [Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)

### Feature testcontainers documentation

- [https://www.testcontainers.org/](https://www.testcontainers.org/)

### Feature jooq documentation

- [Micronaut jOOQ fluent API for typesafe SQL query construction and execution documentation](https://micronaut-projects.github.io/micronaut-sql/latest/guide/index.html#jooq)
